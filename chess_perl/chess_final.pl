#!/usr/bin/perl

use POSIX;
use List::MoreUtils;

############################## PROGRAM CONSTANTS #####################################
$learnmode = 0;
$alearnmode = 0;
$gamemode = 0;
$alfa = 0.5;
$beta = 0.8;
$rewardpoint = 200;
#$reward = 0;
$connectedpoint = 1;
$rookon7point = 1;
# tas tehlike artik kullanilmiyor
$tastehlikepoint = 30;
$checkmatepoint = 5;
$kingdistance1point = 3; # merkezdeki 4 karede ise
$kingdistance2point = 2; # merkez disindaki sirada ise
$kingdistance3point = 1; # bir sonraki sirada ise
$kingdistance4point = 0; # en distaki sirada ise
$gamesayisi = 0;
######################################################################################
undef(%q);
use DBI;
my $database = 'gokce';
my $user = 'gokce';
my $pass = 'qlearning';

my $dsn = 'DBI:mysql:gokce:localhost:3306';

my $dbh = DBI->connect($dsn,$user,$pass) or die "Can’t connect to the DB: $DBI::errstr\n";

$query = "select fen,move,value from qtable";

$sth = $dbh->prepare($query) or print "Veri alınamadı!\n";
$sth->execute;
$rv = $sth->rows;

$sttime = time();
while(@row = $sth->fetchrow_array()) {
                                        $q{$row[0]}{$row[1]} = $row[2];
                                        }
$fntime = time();
$time = $fntime-$sttime;
print "\nSuccesfully read $rv records in $time seconds.\n";
######################################################################################


startposition();
moveselect();

#########################################################  FONKSIYON TANIMLAMALARI #######################################################################






################################## startposition() #############################################################
sub startposition {

$turn = "w";
undef(@whitemovelist);
undef(@blackmovelist);
undef(%board);
$gamelist = '';
undef(@qtable);
$countmove = 0;
$blackmove = 'd8e8';push(@blackmovelist,$blackmove);
$whitemove = 'g1f1';push(@whitemovelist,$whitemove);
$lastfen = ''; # qtable guncellenirken en son fen gerektigi icin kullaniliyor
undef(@lastfenlist);
$reward = 0;
$qvalue = 0;
undef(@sp);
$alearnmode = 0;
undef(@seq);
@mateseq = ("f7a7","d4c5","c5d6","d6e6","e6f6","f6g6","a7a8");
#@mateseq = ("f7a7","d4c5","c5b6","a7d7");
#@mateseq = ("f7a7","d4e4","a7e7");
$mi = 0;
$oi = 0;
if ($q{'6k1/R7/6K1/8/8/8/8/8 w - - 0 1'}{'a7a8'} == 200) {print "$gamesayisi\n";exit;}
$gamesayisi++;


%board = (
#       0    1       2       3       4         5         6       7         8
a => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "-"   ],
b => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "-"   ],
c => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "-"   ],
d => [ "",  "-"  ,  "-"  ,  "-"  ,  "wK"   ,  "-"    ,  "-"  ,  "-"    ,  "-"   ],
e => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "bK"  ],
f => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "wR1"  ,  "-"   ],
g => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "-"   ],
h => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "-"   ]
        );

####################### original 5 pieces table ############################################
#%board = (
#       0    1       2       3       4         5         6       7         8
#a => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "-"   ],
#b => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "-"   ],
#c => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "-"   ],
#d => [ "",  "-"  ,  "-"  ,  "-"  ,  "wK"   ,  "-"    ,  "-"  ,  "-"    ,  "-"   ],
#e => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "-"    ,  "bK"  ],
#f => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "-"    ,  "-"  ,  "wR1"  ,  "-"   ],
#g => [ "",  "-"  ,  "-"  ,  "-"  ,  "-"    ,  "bR"   ,  "-"  ,  "-"    ,  "-"   ],
#h => [ "",  "-"  ,  "-"  ,  "-"  ,  "wR2"  ,  "-"    ,  "-"  ,  "-"    ,  "-"   ]
#        );
############################################################################################



########### SETTING THE WHITE OR BLACK MOVE AND WHITE-BLACK BOARD ####################################################################
# crafty'e veri gonderiyor. evaluation ve automove fonksiyonlari icin gerekli olan move dan onceki tahta pozisyonlari ve onceki move lar set ediliyor.
		foreach $h ( a..h ) {
					foreach $s (1..8) {
								$blackboard{$h}[$s] = $board{$h}[$s];
								$whiteboard{$h}[$s] = $board{$h}[$s];
								$evalboard{$h}[$s] = $board{$h}[$s];
							   }
				    }
$blackboard{e}[8] = '-';
$blackboard{d}[8] = 'bK';
#$whiteboard{h}[1] = '-';
#$whiteboard{g}[1] = 'wK';

$evalboard{h}[8] = $evalboard{h}[4];
$evalboard{h}[4] = '-';

                    }
########################################################################################################################################
######################## END startposition() #######################################################










############################    showtable() sub function  #####################################
sub showtable() {

print "\n\n\n";

# Top string set a .. h
print "\t";
foreach $harf (a..h) {
			print "\e[1;33m$harf\e[0m\t";
			}

print "\n\n\n";
# end Top string ##


#### Table print ##################
for($n = 8 ; $n >= 1 ; $n--) {
		print "\e[1;33m$n\e[0m\t";
			foreach $m (a..h)
					{
					if ( ($move ne '') and ( (split(//, $board{$m}[$n]))[0] eq $turn )  and ( (split(//,$board{$m}[$n]))[1] eq "K") and ( danger_check($turn,'K') == 1 ) or ( $kingd == 1 ) ) {

														if ($turn eq "b") {
																print "\e[1;5;31mb\e[0m";
																print "\e[1;5;31m".((split(//,$board{$m}[$n]))[1])."\e[0m\t";
																   }

														else {
															print "\e[1;5;29mb\e[0m";
															print "\e[1;5;29m".((split(//,$board{$m}[$n]))[1])."\e[0m\t";
														      }

														 					 }

					elsif ( ($move ne '') and ( (split(//, $board{$mv[2]}[$mv[3]]))[0] eq "b") and ($mv[2] eq $m ) and ($mv[3] == $n ) ) {
														print "\e[1;31mb\e[0m";
														print "\e[1;32m".((split(//,$board{$m}[$n]))[1])."\e[0m\t";
														 					 }


					elsif ( ($move ne '') and ( (split(//, $board{$mv[2]}[$mv[3]]))[0] eq "w") and ($mv[2] eq $m ) and ($mv[3] == $n ) ) {
														print "\e[1;29mw\e[0m";
														print "\e[1;32m".((split(//,$board{$m}[$n]))[1]).((split(//,$board{$m}[$n]))[2])."\e[0m\t";
														 					 }


					elsif ( ($move ne '') and ( (split(//, $board{$mv[0]}[$mv[1]]))[0] eq "-") and ($mv[0] eq $m ) and ($mv[1] == $n ) ) {
														print "\e[1;35m".($board{$m}[$n])."\e[0m\t";
														 					 }



					elsif ( (split(//, $board{$m}[$n]))[0] eq "b" ) {
											print "\e[1;31m$board{$m}[$n]\e[0m\t";
	  									        }

					elsif ( (split(//, $board{$m}[$n]))[0] eq "w" ) {
									print "\e[1;29m$board{$m}[$n]\e[0m\t";
										    }

					else { print "$board{$m}[$n]\t"; }

					}

			print "\e[1;33m$n\e[0m\n\n\n\n";

		   	      }

##### END table print ###############


# Bottom string set a .. h
print "\t";
foreach $harf (a..h) {
			print "\e[1;33m$harf\e[0m\t";
			}
# end bottom string set #


print "\n\n\n\n";

			}
################################# END showtable() sub function #####################################












#################################  domove() sub function ############################################
#       move yapan, gercek tahta pozisyonunu degistiren fonksiyon. (çoğu yerde kullanılıyor)        #
#####################################################################################################
sub domove {  #domove($move,$t,'learn')

undef( @check );

foreach(@_ ) {push( @check, $_ );}

$move = @check[0];
$dt = @check[1];
$lr = @check[2];
#print "\nDO MOVE daki move:  $move\n";

$strset = "abcdefgh";
@mv = split(//,$move );

$tasturu = (split(//,$board{$mv[0]}[$mv[1]]))[1];
$tasrengi = (split(//,$board{$mv[0]}[$mv[1]]))[0];


# if legal move (board sınırları içindeki move lar legal move oluyor)
if (  ($tasrengi eq $dt ) and ( index( $strset,$mv[0] ) >= 0 ) and ( index( $strset,$mv[2] ) >= 0) and ( ($mv[1] > 0) and ($mv[1] < 9) ) and ( ($mv[3] > 0) and ($mv[3] < 9) ) ) {


#################### KING'S MOVE ####################################3
		if ($tasturu eq 'K') {
				# print "BIR KING\n";

                $posmoves = posmoves($dt,$mv[0],$mv[1]); #bulundugum yerin possible move listesini alir
                # print "MYMOVE : $mymove\n";
				# print "POSMOVES : $posmoves\n";
				# print "index = ";print index($posmoves,$mymove);
				if (index($posmoves,$move) < 0) {
                                                #print "\n11111111111\n";
                                                illegalmove();
                                                };
                            }
########### END OF KING'S MOVE ############################







########## ROOK'S MOVE ####################################
		if ($tasturu eq 'R') {
				# print "BIR ROOK\n";
                $posmoves = posmoves($dt,$mv[0],$mv[1]);
#				print "MYMOVE : $mymove\n";
#				print "POSMOVES : $posmoves\n";
				# print "index = ";print index($posmoves,$mymove);
				if (index($posmoves,$move) < 0) {
                                                #print "\n22222222222222\n";
                                                illegalmove();
                                                };
 		 		    }
########## END OF ROOK'S MOVE ########################################


# yapacagim move un sahimi tehdit altinda birakip birakmadiginin kontrolu
        if (danger_check($dt,$move) > 0) {
                                            illegalmove();
                                            }



# crafty'e veri gonderirken bir onceki durum ve move gerektigi icin bunlar setleniyor.
########### SETTING THE WHITE OR BLACK MOVE AND WHITE-BLACK BOARD ####################################################################
		foreach $h ( a..h ) {
					foreach $s (1..8) {
								if ($dt eq 'b') {
											$blackboard{$h}[$s] = $board{$h}[$s];
										   }

								else {$whiteboard{$h}[$s] = $board{$h}[$s];};

							   }
				    }
#######################################################################################################################


# qtable kayitlari yazilirken move oncesindeki durum gerektiginden $lastfen degeri move yapilmadan once setleniyor.
$lastfen = fen('w','board');


if ($dt eq 'w') {$ct = 'b';$checkmate = 0;}
else {$ct = 'w';}

$dcheck = danger_check($ct,$move);

if ($dt eq 'w') {
                    $lastfen2 = fen('w','board');
                    push(@lastfenlist2,$lastfen2);

                    if ($dcheck > 0) {
                                        $checkmated = 1;
                                        $checkmate += $checkmatepoint;
                                        }

                    else {$checkmated = 0;}
                }

else {
        if ($dcheck > 0) {$checkmated = 1;}
        else {$checkmated = 0;}
        }


push(@fenlist,$lastfen);
# gercek tahta uzerinde move gerceklestiriliyor.
		$board{$mv[2]}[$mv[3]] = $board{$mv[0]}[$mv[1]];
		$board{$mv[0]}[$mv[1]] = "-";

		if ($dt eq "w") {

##########################################
$ekleseq = "$lastfen:$move";
push(@seq,$ekleseq);
##########################################


                    push(@whitemovelist,$move); #thirdrepetition icin
#                    print "\nBUDUR: @whitemovelist[-1]\n";
					$turn = "b";
					$whitemove = $move; #crafty nin kullandigi veri
					push(@fenlistwhite,fen('w','board'));
				   }
			else {
                    push(@blackmovelist,$move); #thirdrepetition icin
#                    print "\nBUDUR: @blackmovelist[-1]\n";
					$turn = "w";
					$blackmove = $move; #crafty nin kullandigi veri
					push(@fenlistblack,fen('w','board'));
			      };

$countmove++;
# eski kayit dosyalarinin yazilmasi (gamelist_saved....)
if ($turn eq 'w' and $countmove > 5) {$gamelist .= $blackmovelist[-5].":"}

##############################################################
#if ($lr eq 'learn') {legalmove('learn');}
#else {legalmove();}


	}
# end if legal move

else {
#        print "\n33333333333333333\n";
        open(BUGS,"+>> bug_moves.txt");
        print BUGS fen('b').": $blackmove <-->".fen('w').": $whitemove <-->"."$turn->$move\n";
        close(BUGS);
        #exit;
#        print "\nTASRENGI $tasrengi - $turn\n";
        illegalmove();
      };

		}
#################################  END domove() sub function #######################################



















############### # games ( $turn , $yazma , 'evalboard' ); #######################
#  @veri[0]: sira kimde ise butun gecerli move listesi                          #
#  @veri[1]: tahtadaki toplam tas sayisi (thirdrepetition)                      #
#################################################################################
sub games {   # games ( $turn , $yazma , 'evalboard' );

	undef( @check );

	foreach(@_ ) {push( @check, $_ );}

	$t = @check[0];
	$yazma = @check[1];
	$table = @check[2];

	if ($table eq '') {$table = 'board';};

################ SET CHECKGAMESBOARD #########################################
#$checkgamesboard{$gh}[$gs] = ${$table}{$gh}[$gs];
# cok amacli sanal tahta
%checkgamesboard = %{$table};
##############################################################################

$allmoves = "";
$tascount = 0;

# $turn'e ait tum taslar icin possible move listesi ve tahtadaki toplam tas sayisi cikarilir.
foreach $gh ( a..h ) {
			foreach $gs ( 1..8 ) {
                                        if ( $checkgamesboard{$gh}[$gs] ne '-' ) {$tascount += 1;};
                                        if ( (split(//,$checkgamesboard{$gh}[$gs]))[0] eq $t ) {

                                                                                                if ($yazma == 1 ) {
                                                                                                                    print "$checkgamesboard{$gh}[$gs] moves: ($gh$gs) ->  ";
                                                                                                                    print posmoves($t,$gh,$gs,$table);
                                                                                                                    print "\n";
                                                                                                                    };

                                                                                                $allmoves .= posmoves($t,$gh,$gs,$table);

                                                                                                }
                                }

                    }

        undef(@veri);
        push(@veri,$allmoves);
        push(@veri,$tascount);

		return @veri;
	   }
############### END OF games($turn , $yazma , 'evalboard' ); ####################



















############### posit("w","K","1","a","e7",'evalboard') #################################
#   tahtadaki herhangi bir tasin pozisyonunu bulur (wK->e8 gibi)
#########################################################################################
sub posit { #posit("w","K","1","a","e7",'evalboard')

	$p = "";
	 my($pturn,$type,$num,$sutun,$pmove,$table) = @_;

if ($table eq '') {$table = 'board';};

#########################################################################################
%pboard = %{$table};
#########################################################################################

if ( $type eq 'R' ) {

                    foreach $h ( a..h ) {

                                foreach $s (1..8) {

                                        chomp( $posk = posmoves ($pturn,$h,$s,$table) );

#                                        print "\nPOSK = $posk\n";chomp($dur = <STDIN>);

                                        if ( $pmove ne '' and $sutun eq '' ) {
                                                                                if ( ((split(//,$pboard{$h}[$s]))[0] eq $pturn ) and ((split(//,$pboard{$h}[$s]))[1] eq $type ) and $posk =~ m/$pmove/g ) {
                                                                                                                                                                                                            $p .= $h; $p .= $s;
                                                                                                                                                                                                           }
                                                                               }

                                        elsif ( $sutun ne '' and $pmove eq '' ) {
                                                                                # burada $sutun o sutunda bulunan kaleyi bulmak icin verilen sutun bilgisi.
                                                                                if ($sutun =~ m/[a-h]]/ ) {
                                                                                                            if ( ((split(//,$pboard{$h}[$s]))[0] eq $pturn ) and ((split(//,$pboard{$h}[$s]))[1] eq $type ) and $sutun eq $h ) {$p .= $h; $p .= $s;};
                                                                                                        }
                                                                                # burada ise $sutun o satirda bulunan kaleyi bulmak icin verilen satir bilgisi.
                                                                                else {
                                                                                        if ( ((split(//,$pboard{$h}[$s]))[0] eq $pturn ) and ((split(//,$pboard{$h}[$s]))[1] eq $type ) and $sutun == $s ) {$p .= $h; $p .= $s;};
                                                                                        }
                                                                                }

                                        elsif ( $pmove ne '' and $sutun ne '' ) {
                                                                                if ( ((split(//,$pboard{$h}[$s]))[0] eq $pturn ) and ((split(//,$pboard{$h}[$s]))[1] eq $type ) and $sutun eq $h and  $posk =~ m/$pmove/ ) {$p .= $h; $p .= $s;};
                                                                                }

                                        elsif ( $num eq '1' or $num eq '2') {
#										                                        print "\nAL SANA NUM : $num - $h:$s\n";
                                                                                if ( ((split(//,$pboard{$h}[$s]))[0] eq $pturn ) and ((split(//,$pboard{$h}[$s]))[1] eq $type ) and $num eq (split(//,$pboard{$h}[$s]))[2] ) {$p .= $h; $p .= $s;};
                                                                             }


                                        else {
                                                if ( ((split(//,$pboard{$h}[$s]))[0] eq $pturn ) and ((split(//,$pboard{$h}[$s]))[1] eq $type ) ) {$p .= $h; $p .= $s;};
                                              }



                                                    }

                                        }
                        }


elsif ( $type eq 'K' ) {
                        foreach $h ( a..h ) {

                                            foreach $s (1..8) {
                                                                if ( ((split(//,$pboard{$h}[$s]))[0] eq $pturn ) and ((split(//,$pboard{$h}[$s]))[1] eq $type ) ) {$p .= $h; $p .= $s;};
                                                                }
                                            }
                        }
return $p;
        }
############### END posit("w","K","1","a","e7",'evalboard') #############################





















################## posmoves ($t,$cx,$cy,$table) ###############################################
sub posmoves { #posmoves($t,$cx,$cy,$table)


#		my( $turn, $type, $num )  = @_;
		my($t,$cx,$cy,$table) = @_;

if ($table eq '') {$table = 'board';}

$type = (split (//,${$table}{$cx}[$cy]))[1];

if ($type eq 'K') {


# KING VARSA POS MOVES

		$pm = $cx.$cy;
		$strset = "abcdefgh";
		$pstr = '';
		$nstr = '';
		$pint = 0;
		$nint = 0;
		if ($cx ne 'a') {$pstr = substr($strset, (index($strset,$cx)-1),1 );};#print "\nYENI PREV STR : $pstr\n";
		if ($cx ne 'h') {$nstr = substr($strset, (index($strset,$cx)+1),1 );};#print "\nYENI NEXT STR : $nstr\n";
		if ($cy > 1) {$pint = $cy-1;};#print "\npINT : $pint\n";
		if ($cy < 8) {$nint = $cy+1;};#print "\nnINT : $nint\n";
		$posmoves = '';

		$movecheck = $pm.$cx.$pint;
		if ( (split(//,${$table} {$cx}[$pint]))[0] ne $t and danger_check($t,$movecheck,$table) == 0 )  {
									if ($pint >= 1) {$posmoves .= "$pm$cx$pint";};
														  };# print "A $posmoves\n";

		$movecheck = $pm.$cx.$nint;
		if ( (split(//,${$table} {$cx}[$nint]))[0] ne $t and danger_check($t,$movecheck,$table) == 0 )  {
									if ($nint >= 1) {$posmoves .= "$pm$cx$nint";};
													};# print "B $posmoves\n";

		$movecheck = $pm.$pstr.$pint;
		if ( $pstr ne '' and (split(//,${$table} {$pstr}[$pint]))[0] ne $t and danger_check($t,$movecheck,$table) == 0 )  {
									if ( (index( $strset,$pstr ) >= 0 ) and ($pint >= 1) ) {$posmoves .= "$pm$pstr$pint";};
													};# print "C $posmoves\n";


		$movecheck = $pm.$pstr.$cy;
		if ( $pstr ne '' and (split(//,${$table} {$pstr}[$cy]))[0] ne $t and danger_check($t,$movecheck,$table) == 0 )  {
									if ( index( $strset,$pstr ) >= 0 ) {$posmoves .= "$pm$pstr$cy";};
												   		};# print "C $posmoves\n";

		$movecheck = $pm.$pstr.$nint;
		if ( $pstr ne '' and (split(//,${$table} {$pstr}[$nint]))[0] ne $t and danger_check($t,$movecheck,$table) == 0 )   {
									if ( (index( $strset,$pstr ) >= 0 ) and ($nint >= 1) ) {$posmoves .= "$pm$pstr$nint";};
									 };# print "D $posmoves\n";

		$movecheck = $pm.$nstr.$pint;
		if ( $nstr ne '' and (split(//,${$table} {$nstr}[$pint]))[0] ne $t and danger_check($t,$movecheck,$table) == 0 )  {
														if ( (index( $strset,$nstr ) >= 0 ) and ($pint >= 1) ) {$posmoves .= "$pm$nstr$pint";};
														 };# print "E $posmoves\n";

		$movecheck = $pm.$nstr.$cy;
		if ( $nstr ne '' and (split(//,${$table} {$nstr}[$cy]))[0] ne $t and danger_check($t,$movecheck,$table) == 0 )  {

														if ( index( $strset,$nstr ) >= 0 ) {$posmoves .= "$pm$nstr$cy";};
														 };# print "E $posmoves\n";

		$movecheck = $pm.$nstr.$nint;
		if ( $nstr ne '' and (split(//,${$table} {$nstr}[$nint]))[0] ne $t and danger_check($t,$movecheck,$table) == 0 )   {
									if ( (index( $strset,$nstr ) >= 0 ) and ($nint >= 1) ) {$posmoves .= "$pm$nstr$nint";};
									};# print "F $posmoves\n";

		chomp( $posmoves );

		return $posmoves;
}





elsif ($type eq 'R') {

		$pm = $cx.$cy;
		$strset = "abcdefgh";
		$pstr = "";
		$nstr = "";
		$pint = "";
		$nint = "";
		$pstr = substr($strset, (index($strset,$cx)-1),1 );#print "YENI PREV STR : $pstr\n";
		$nstr = substr($strset, (index($strset,$cx)+1),1 );#print "YENI NEXT STR : $nstr\n";
		$pint = $cy-1;#print "PINT : $pint\n";
		$nint = $cy+1;#print "NINT : $nint\n";
		$posmoves = "";

				# print "BIR ROOK\n";
				$pstr = '';
				$pint = '';

				$near = '';
				foreach(a..$cx) {if ( ${$table}{$_}[$cy] ne '-' and $_ ne $cx) {$near = $_;};};
				if ( $near eq '' ) {$near = "a";};

				foreach $h ($near..$cx) {
                                        $movecheck = $pm.$h.$cy;
                                        if ($h ne $cx and (split(//,${$table}{$h}[$cy]))[0] ne (split(//,${$table}{$cx}[$cy]))[0] and danger_check($t,$movecheck,$table) == 0 ) {$pstr .= $h;};
                                        };


				$near = 'h';
				foreach($cx..h) {if ( ${$table}{$_}[$cy] ne '-' and $_ ne $cx and $_ lt $near ) {$near = $_;};};

				foreach $h ($cx..$near) {
                                        $movecheck = $pm.$h.$cy;
                                        if ($h ne $cx and (split(//,${$table}{$h}[$cy]))[0] ne (split(//,${$table}{$cx}[$cy]))[0] and danger_check($t,$movecheck,$table) == 0 ) {$pstr .= $h;};
                                        };

				$nearint = "";
				foreach(1..$cy) {if ( ${$table}{$cx}[$_] ne "-" and $_ ne $cy) {$nearint = $_;};};
				if ( $nearint eq '' ) {$nearint = 1;};

				foreach $h ($nearint..$cy) {
                                            $movecheck = $pm.$cx.$h;
                                            if ($h != $cy and (split(//,${$table}{$cx}[$h]))[0] ne (split(//,${$table}{$cx}[$cy]))[0] and danger_check($t,$movecheck,$table) == 0 ) {$pint .= $h;};
                                            };

				$nearint = 8;
				foreach($cy..8) {if ( ${$table}{$cx}[$_] ne "-" and $_ ne $cy and $_ < $nearint) {$nearint = $_;};};
				if ( $nearint eq '' ) {$nearint = 8;};

				foreach $h ($cy..$nearint) {
                                            $movecheck = $pm.$cx.$h;
                                            if ($h ne $cy and (split(//,${$table}{$cx}[$h]))[0] ne (split(//,${$table}{$cx}[$cy]))[0] and danger_check($t,$movecheck,$table) == 0 ) {$pint .= $h;};
                                            };

				$posmoves = "";
				foreach ( split(//,$pstr ) ) {$posmoves .= $pm.$_.$cy;}
				foreach ( split(//,$pint ) ) {$posmoves .= $pm.$cx.$_;}

				chomp( $posmoves );

				return $posmoves;
}

	      }
############################### END OF posmoves () #############################################














##############################  danger_check($t,$move,$table) ##################################
sub danger_check { #danger_check ( $t , $move ,'evalboard' )

		$kd = 0;

		undef( @check );

        foreach(@_ ) {push( @check, $_ );}

        $t = @check[0];
        $table = @check[2];
        $alomove = @check[1];
        if ($table eq ''){$table = 'board';};

#####################################################################################################
		foreach $h ( a..h ) {
					foreach $s (1..8) {
								$checkboarddanger{$h}[$s] = ${$table}{$h}[$s];
                                        }
                            }
#####################################################################################################

		@dmv = split(//,@check[1]);

		if (@dmv[3] <= 8 and @dmv[3] >= 1) {
 							$checkboarddanger {@dmv[2]}[@dmv[3]] = $checkboarddanger{@dmv[0]}[@dmv[1]];
							$checkboarddanger {@dmv[0]}[@dmv[1]] = "-";
						    }

		# king koordinati bulunur
		foreach $h ( a..h ) {
					foreach $s (1..8) {
								if ( ((split(//,$checkboarddanger{$h}[$s]))[0] eq $t ) and ((split(//,$checkboarddanger{$h}[$s]))[1] eq 'K') ) {@kc[0] = $h; @kc[1] = $s;};
							   }
				    }

		$nearkingstr = "";
		foreach(a..@kc[0]) {if ( $checkboarddanger{$_}[@kc[1]] ne "-" and $_ ne @kc[0]) {$nearkingstr = $_;};};
		if ( (split(//,$checkboarddanger{$nearkingstr}[@kc[1]]))[0] ne $t and  (split(//,$checkboarddanger{$nearkingstr}[@kc[1]]))[1] eq 'R' ) {$kd = 1;};




        $nearkingstr = 'h';
		foreach(@kc[0]..h) {
                            if ( $checkboarddanger{$_}[@kc[1]] ne '-' and $_ ne @kc[0] and $_ lt $nearkingstr) {$nearkingstr = $_;};
                            };
		if ( $nearkingstr eq '' ) {$nearkingstr = 'h';};
		if ( (split(//,$checkboarddanger{$nearkingstr}[@kc[1]]))[0] ne $t and ((split(//,$checkboarddanger{$nearkingstr}[@kc[1]]))[1] eq 'R') ) {$kd = 1;};


		$nearkingint = 1;
		foreach(1..@kc[1]) {if ( $checkboarddanger{@kc[0]}[$_] ne "-" and $_ ne @kc[1]) {$nearkingint = $_;};};
		if ( (split(//,$checkboarddanger{@kc[0]}[$nearkingint]))[0] ne $t and  (split(//,$checkboarddanger{@kc[0]}[$nearkingint]))[1] eq 'R' ) {$kd = 1;};


		$nearkingint = 8;
		foreach(@kc[1]..8) {if ( $checkboarddanger{@kc[0]}[$_] ne '-' and $_ ne @kc[1] and $_ < $nearkingint) {$nearkingint = $_;};};
		if ( (split(//,$checkboarddanger{@kc[0]}[$nearkingint]))[0] ne $t and (split(//,$checkboarddanger{@kc[0]}[$nearkingint]))[1] eq 'R' ) {$kd = 1;};


        $dpstr = @kc[0];
        $dnstr = @kc[0];
        $dpint = @kc[1];
        $dnint = @kc[1];
		if (@kc[0] ne 'a') {$dpstr = substr($strset, (index($strset,@kc[0])-1),1 );};
		if (@kc[0] ne 'h') {$dnstr = substr($strset, (index($strset,@kc[0])+1),1 );};
		if (@kc[1] > 1 ) {$dpint = @kc[1]-1;};
		if (@kc[1] < 8 ) {$dnint = @kc[1]+1;};

		foreach $a ( $dpstr..$dnstr ) {
					foreach $b ( $dpint..$dnint ) {
								if ( (split(//,$checkboarddanger{$a}[$b]))[1] eq 'K' and (split(//,$checkboarddanger{$a}[$b]))[0] ne $t ) {
                                                                                                                                            $kd = 1;
                                                                                                                                            };
								     }
					     }
	return $kd;
		}
#################################### END OF danger_check ######################################
















################ automove($t) ##################################
sub automove { #automove($t)

undef( @check );

foreach(@_ ) {push(@check,$_);}

$t = @check[0];

$fen = fen($t);

open (INPUT, "+> gokce.dat");
print INPUT "ponder off\n";
print INPUT "egtb\n";
print INPUT "setboard $fen\n";
print INPUT "\n";
if ($t eq 'b') {print INPUT "$whitemove\n";}
elsif ($t eq 'w') {print INPUT "$blackmove\n";}

close (INPUT);

print "\nPondering...... ";

$egtb = `./crafty`;

open (EGTBMOVE, "< gokce2.dat");

while ( chomp( $line = <EGTBMOVE> ) ) {
                                $newmove = $line;
                                        }
close (EGTBMOVE);

$newmove = convertmove($newmove);
if ($t eq 'w') {print "\e[0;0;44m$newmove\e[0m\n";}
else {print "\e[0;0;41m$newmove\e[0m\n";}

domove($newmove,$t);
	}
################ END OF automove ($t) ##########################










######################### convertmove($craftymove) ############################
#  crafty den gelen (ornegin Re7) formattaki move'u e6e7 formatina donusturuyor.
###############################################################################
sub convertmove { #convertmove($craftymove)

undef( @check );

foreach(@_ ) {push( @check, $_ );}

$movenew = '';

$craftymove = @check[0];

$craftymove =~ s/x//g;
$craftymove =~ s/\+//g;
$craftymove =~ s/\#//g;
$craftymove =~ s/\?//g;
$craftymove =~ s/\?\?//g;
$craftymove =~ s/\!//g;

chop( $craftymove );

@c = split(//,$craftymove );

if ( length($craftymove) > 3 ) {
                                $movenew = posit($turn,"$c[0]",'',$c[1]).$c[-2].$c[-1];
                                }

else {$movenew = posit( $turn,@c[0],3,'', $c[-2].@c[-1]).$c[-2].$c[-1];}
return $movenew;
	}
######################### END convertmove ($craftymove) #######################















#################### fen($fenturn,$table) ###################################
sub fen { #fen($fenturn,$table)

undef( @check );
undef(%evalpos);

foreach(@_ ) {push( @check, $_ );}

$fenturn = @check[0];
$table = @check[1];

if ($table eq '') {
                    if ($fenturn eq 'b') {$fenboard = 'whiteboard';}
                    elsif ($fenturn eq 'w') {$fenboard = 'blackboard';}
                    }
else {$fenboard = $table;};

$fen = '';

for ($s = 8 ; $s >= 1 ; $s--) {

                                $bosluk = 0;
                                foreach $h (a..h) {

                                                    if ( ${$fenboard}{$h}[$s] eq '-' ) {
                                                                                        $bosluk += 1;
                                                                                        if ( $h eq 'h' and $bosluk > 0 ) {$fen .= $bosluk;$bosluk = 0;}
                                                                                        }

                                                    else {

                                                            if ( $bosluk > 0 ) {$fen .= $bosluk;$bosluk = 0;};
                                                            if ( ${$fenboard}{$h}[$s] eq 'wK' ) {$fen .= 'K';$bosluk = 0;}
                                                            elsif ( ${$fenboard}{$h}[$s] eq 'wR1' ) {$fen .= 'R';$bosluk = 0;}
                                                            elsif ( ${$fenboard}{$h}[$s] eq 'wR2' ) {$fen .= 'R';$bosluk = 0;}
                                                            elsif ( ${$fenboard}{$h}[$s] eq 'bK' ) {$fen .= 'k';$bosluk = 0;}
                                                            elsif ( ${$fenboard}{$h}[$s] eq 'bR' ) {$fen .= 'r';$bosluk = 0;}

                                                            }

                                                    }

			if ( $s ne '1' ) {$fen .= '/';}

                                }

if ($fenturn eq 'w' and $table eq '') {$fen .= " b - - 0 1";}
elsif ($fenturn eq 'b' and $table eq '') {$fen .= " w - - 0 1";}
elsif ($table ne '') {$fen .= " $fenturn - - 0 1";}
return $fen;

	}
######################################################################











#####################  Ŕt,$emove) #########################################################
sub ef { #ef($t,$emove)

my($t,$emove)  = @_;

@fm = split(//,$emove);

$efpuan = $whitemovepoints{$whitemovelist[-1]}+$checkmate;

$checkmate = 0;

return $efpuan;

        }
##################### END of ef() ############################################################














############################ alef($t) #################################################
# all possible moves icin tum value listesini @allvalues dizisine atip gonderir.
#######################################################################################
sub alef { #alef($t)

my($t)  = @_;

undef(@allvalues);
$efmoves = (games ($t,0))[0];

for ($i=0 ; $i < length($efmoves) ; $i+=4 ) {
                                                $fmove = substr($efmoves,$i,4);
                                                ef($t,$fmove);
                                                push(@allvalues,ef($t,$fmove));
                                                }

return @allvalues;

        }
############################ END alef($t) #############################################











############################ getqmoves($t) #############################################################################
# alef() in yaptigi ise ek olarak hangi move un hangi value ye sahip oldugunu gosteren bir hash yapisi uretip gonderir.
########################################################################################################################
sub getqmoves { #getqmoves($t)

my( $t )  = @_;

undef(@qmoves);
%qmoves = ();
$efmoves = (games ($t,0))[0];

for ($i=0 ; $i < length($efmoves) ; $i+=4 ) {
                                                $fmove = substr($efmoves,$i,4);
                                                $qmoves{$fmove} = ef($t,$fmove);
                                                }
return %qmoves

        }
############################ END getqmoves($t) #################################














############################ getposmoves($t) ######################################
# artik kullanilmiyor, kaldirilabilir
###################################################################################
sub getposmoves { #getposmoves($t)

my($t)  = @_;

undef(@allgpmoves);
$gpmoves = (games ($t,0))[0];

for ($i=0 ; $i < length($gpmoves) ; $i+=4 ) {
                                                $gmove = substr($gpmoves,$i,4);
                                                push(@allgpmoves,$gmove);
                                                }
return @allgpmoves;
                }
############################ END getposmoves($t) ##################################









########################### qtable() ################################################
sub qtable { #qtable()

my($ch,$mate) = @_;

$currentfen = fen('w','board');

#print "Pickmove: $pickmove , QValue : $qvalue\n";
#$dur = <STDIN>;

if ($countmove < 1) {push(@qtable,0);}
else {

                                                                                    ##################################################################################
                                                                                    #print "CURFEN : $currentfen\n";
                                                                                    @next_state_all_moves = getposmoves('w');
                                                                                    #print "SONRAKI MOVE SAYISI : ".(scalar(@next_state_all_moves))."\n";
                                                                                    undef(%nextmovespastvalues);
                                                                                    %nextmovespastvalues = ();
                                                                                    foreach (@next_state_all_moves) {
                                                                                                                    #print "$_\n";
                                                                                                                    if ( defined($q{$currentfen}{$_}) ) {
                                                                                                                                                        #print "AAA : $_ : $q{$currentfen}{$_}\n";
#                                                                                                                                                       $nextmovespastvalues{$currentfen}{$_} = $q{$currentfen}{$_};
                                                                                                                                                        $nextmovespastvalues{$_} = $q{$currentfen}{$_};
                                                                                                                                                        };
                                                                                                                    };

                                                                                    @keynext = (sort hashValueAscendingNum2 (keys(%nextmovespastvalues)));
                                                                                    $max_v_next_state = $nextmovespastvalues{$keynext[-1]};
                                                                                    #if ($max_v_next_state eq '') {$max_v_next_state = 0;};

                                                                                    $oldq = $q{$lastfenlist2[-1]}{$whitemovelist[-1]};
                                                                                    ##################################################################################

        # eger qtable da fen ve move ikilisi icin daha once kayit yoksa yeni bir satir olarak veri giriliyor. $q{fen}{move} = q
        if ( $q{$lastfenlist2[-1]}{$whitemovelist[-1]} eq '') {
                                                                if ($mate eq 'mate') {

                                                                                    $newq = (1-$alfa)*$whitemovepoints{$whitemovelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                                                                                    print "Adding new record ($lastfenlist2[-1]:$whitemovelist[-1])...(MATE)\n";#$dur = <STDIN>;
                                                                                    $q{$lastfenlist2[-1]}{$whitemovelist[-1]} = $newq;
                                                                                    #print "$lastfenlist2[-1]:$whitemovelist[-1]  --> ilk defa yazildi.\n";
                                                                                    $addquery = "insert into qtable(id,fen,move,value) values('','$lastfenlist2[-1]','$whitemovelist[-1]','$newq')";
                                                                                    $adh = $dbh->prepare($addquery);
                                                                                    $adh->execute or print "Can't insert record!\n";


                                                                                    $readquery = "select value from qtable where fen='$lastfenlist2[-1]' and move='$whitemovelist[-1]'";
                                                                                    $rdh = $dbh->prepare($readquery);
                                                                                    $rdh->execute or print "Can't read!\n";
                                                                                    $a = $rdh->fetchrow;
                                                                                    print "A: $a\n";

                                                                                    $search = "$lastfenlist2[-1]:$whitemovelist[-1]";
                                                                                    my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                                                                    print "kayitin oyun sira indexi: $index\n";

                                                                                    if ($index > 0) {
                                                                                    print "Ayrica asagidakiler guncellenecektir:\n";

                                                                                    for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                                                                                print "$seq[$i]\n-------------\n";
                                                                                                                print "\nNext State : ".($seq[$i+1])."\n";
                                                                                                                @thisarr = split(/:/,$seq[$i]);
                                                                                                                @nextarr = split(/:/,$seq[$i+1]);
                                                                                                                print "Nextfen : $nextarr[0]\n";

                                                                                                                $maxvquery = "select fen,move,value from qtable where fen='$nextarr[0]' order by value desc limit 1";
                                                                                                                $mdh = $dbh->prepare($maxvquery);
                                                                                                                $mdh->execute or print "Can't read!\n";
                                                                                                                @a = $mdh->fetchrow_array();

                                                                                                                print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                                                                $max_v_next_state1 = $a[2];
                                                                                                                $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                                                                print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                                                                $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                                                                $updatequery = "update qtable set value='$newq' where fen='$thisarr[0]' and move='$thisarr[1]'";
                                                                                                                $udh = $dbh->prepare($updatequery);
                                                                                                                $udh->execute or print "Can't update!\n";

                                                                                                                }
                                                                                                    }


                                                                                    open(yeni,"+>> qtable.db");
                                                                                    print yeni "$lastfenlist2[-1]:$whitemovelist[-1]:$q{$lastfenlist2[-1]}{$whitemovelist[-1]}\n";
                                                                                    $komut = `echo 'Adding ---> $lastfenlist2[-1]:$whitemovelist[-1]:$q{$lastfenlist2[-1]}{$whitemovelist[-1]}' >> added_list`;
                                                                                    #print "Yeni Q = $lastfenlist2[-1] : $whitemovelist[-1] : $q{$lastfenlist2[-1]}{$whitemovelist[-1]}\n";
                                                                                    close(yeni);
                                                                                    }
                                                                elsif ($mate eq 'draw') {

                                                                                    $newq = (1-$alfa)*$whitemovepoints{$whitemovelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                                                                                    print "Adding new record ($lastfenlist2[-1]:$whitemovelist[-1])...(DRAW)\n";#$dur = <STDIN>;
                                                                                    $q{$lastfenlist2[-1]}{$whitemovelist[-1]} = $newq;
                                                                                    #print "$lastfenlist2[-1]:$whitemovelist[-1]  --> ilk defa yazildi.\n";
                                                                                    $addquery = "insert into qtable(id,fen,move,value) values('','$lastfenlist2[-1]','$whitemovelist[-1]','$newq')";
                                                                                    $adh = $dbh->prepare($addquery);
                                                                                    $adh->execute or print "Can't insert!\n";

                                                                                    $readquery = "select value from qtable where fen='$lastfenlist2[-1]' and move='$whitemovelist[-1]'";
                                                                                    $rdh = $dbh->prepare($readquery);
                                                                                    $rdh->execute or print "Can't read!\n";
                                                                                    $a = $rdh->fetchrow;
                                                                                    print "B: $a\n";

                                                                                    $search = "$lastfenlist2[-1]:$whitemovelist[-1]";
                                                                                    my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                                                                    print "kayitin oyun sira indexi: $index\n";

                                                                                    if ($index > 0) {
                                                                                    print "Ayrica asagidakiler guncellenecektir:\n";

                                                                                    for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                                                                                print "$seq[$i]\n-------------\n";
                                                                                                                print "\nNext State : ".($seq[$i+1])."\n";
                                                                                                                @thisarr = split(/:/,$seq[$i]);
                                                                                                                @nextarr = split(/:/,$seq[$i+1]);
                                                                                                                print "Nextfen : $nextarr[0]\n";

                                                                                                                $maxvquery = "select fen,move,value from qtable where fen='$nextarr[0]' order by value desc limit 1";
                                                                                                                $mdh = $dbh->prepare($maxvquery);
                                                                                                                $mdh->execute or print "Can't read!\n";
                                                                                                                @a = $mdh->fetchrow_array();

                                                                                                                print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                                                                $max_v_next_state1 = $a[2];
                                                                                                                $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                                                                print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                                                                $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                                                                $updatequery = "update qtable set value='$newq' where fen='$thisarr[0]' and move='$thisarr[1]'";
                                                                                                                $udh = $dbh->prepare($updatequery);
                                                                                                                $udh->execute or print "Can't update!\n";

                                                                                                                }
                                                                                                    }


                                                                                    open(yeni,"+>> qtable.db");
                                                                                    print yeni "$lastfenlist2[-1]:$whitemovelist[-1]:$q{$lastfenlist2[-1]}{$whitemovelist[-1]}\n";
                                                                                    $komut = `echo 'Adding ---> $lastfenlist2[-1]:$whitemovelist[-1]:$q{$lastfenlist2[-1]}{$whitemovelist[-1]}' >> added_list`;
                                                                                    #print "Yeni Q = $lastfenlist2[-1] : $whitemovelist[-1] : $q{$lastfenlist2[-1]}{$whitemovelist[-1]}\n";
                                                                                    close(yeni);
                                                                                        }
                                                                else {
                                                                        $newq = (1-$alfa)*$whitemovepoints{$whitemovelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                                                                        #$newq = $whitemovepoints{$whitemovelist[-1]};
                                                                        print "Adding new record ($lastfenlist2[-1]:$whitemovelist[-1])...\n";#$dur = <STDIN>;
                                                                        $q{$lastfenlist2[-1]}{$whitemovelist[-1]} = $newq;
                                                                        #print "$lastfenlist2[-1]:$whitemovelist[-1]  --> ilk defa yazildi.\n";
                                                                        $addquery = "insert into qtable(id,fen,move,value) values('','$lastfenlist2[-1]','$whitemovelist[-1]','$newq')";
                                                                        $adh = $dbh->prepare($addquery);
                                                                        $adh->execute or print "Can't insert!\n";

                                                                        $readquery = "select value from qtable where fen='$lastfenlist2[-1]' and move='$whitemovelist[-1]'";
                                                                        $rdh = $dbh->prepare($readquery);
                                                                        $rdh->execute or print "Can't read!\n";
                                                                        $a = $rdh->fetchrow;
                                                                        print "C: $a\n";

                                                                        $search = "$lastfenlist2[-1]:$whitemovelist[-1]";
                                                                        my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                                                        print "kayitin oyun sira indexi: $index\n";

                                                                        if ($index > 0) {
                                                                        print "Ayrica asagidakiler guncellenecektir:\n";


                                                                        for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                                                                    print "$seq[$i]\n-------------\n";
                                                                                                    print "\nNext State : ".($seq[$i+1])."\n";
                                                                                                    @thisarr = split(/:/,$seq[$i]);
                                                                                                    @nextarr = split(/:/,$seq[$i+1]);
                                                                                                    print "Nextfen : $nextarr[0]\n";

                                                                                                    $maxvquery = "select fen,move,value from qtable where fen='$nextarr[0]' order by value desc limit 1";
                                                                                                    $mdh = $dbh->prepare($maxvquery);
                                                                                                    $mdh->execute or print "Can't read!\n";
                                                                                                    @a = $mdh->fetchrow_array();

                                                                                                    print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                                                    $max_v_next_state1 = $a[2];
                                                                                                    $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                                                    print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                                                    $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                                                    $updatequery = "update qtable set value='$newq' where fen='$thisarr[0]' and move='$thisarr[1]'";
                                                                                                    $udh = $dbh->prepare($updatequery);
                                                                                                    $udh->execute or print "Can't update!\n";

                                                                                                    }
                                                                                        }


                                                                        open(yeni,"+>> qtable.db");
                                                                        print yeni "$lastfenlist2[-1]:$whitemovelist[-1]:$q{$lastfenlist2[-1]}{$whitemovelist[-1]}\n";
                                                                        $komut = `echo 'Adding ---> $lastfenlist2[-1]:$whitemovelist[-1]:$q{$lastfenlist2[-1]}{$whitemovelist[-1]}' >> added_list`;
                                                                        #print "Yeni Q = $lastfenlist2[-1] : $whitemovelist[-1] : $q{$lastfenlist2[-1]}{$whitemovelist[-1]}\n";
                                                                        close(yeni);
                                                                        }

                                                                }
        else {
                if ($mate eq 'mate') {

                                    print "Updating record ($lastfenlist2[-1]:$whitemovelist[-1])...(MATE)\n";#$dur = <STDIN>;
                                    $newq = (1-$alfa)*$q{$lastfenlist2[-1]}{$whitemovelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                                    $q{$lastfenlist2[-1]}{$whitemovelist[-1]} = $newq;
#                                   $q{$lastfenlist2[-1]}{$whitemovelist[-1]} = (1-$alfa)*$q{$lastfenlist2[-1]}{$whitemovelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                                    #print "$lastfenlist2[-1]:$whitemovelist[-1]  --> kayit guncellendi.\n";
                                    #print "Old: $oldq, New: $q{$lastfenlist2[-1]}{$whitemovelist[-1]}";
                                    $oldline = "$lastfenlist2[-1]:$whitemovelist[-1]:$oldq";
                                    $oldline1 = $oldline;
                                    $newline = "$lastfenlist2[-1]:$whitemovelist[-1]:$newq";
                                    $newline1 = $newline;

                                    $oldline =~ s/\//\\\//g;
                                    $newline =~ s/\//\\\//g;

                                    $komut1 = `mv qtable.db tempdb.db`;
                                    $sed = `sed 's/$oldline/$newline/g' tempdb.db > qtable.db`;

#                                    $komut = `echo 'Updating ($oldline) ---> ($newline) MAX: $max_v_next_state' >> updated_list`;
                                    $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list`;
#                                   $komut = `echo '========================================================================================================' >> updated_list`;
                                    $komut = `echo 'Updating ($oldline1) (MATE)...' >> updated_list`;
                                    $komut = `echo '\e[1;29mOldQ\e[0m = \e[1;41m$oldq\e[0m' >> updated_list`;
                                    $komut = `echo '-------------------------------------' >> updated_list`;
                                    $komut = `echo 'Previously visited (NextState,VisitedMoves) :' >> updated_list`;
                                    $komut = `echo '-------------------------------------' >> updated_list`;
                                    foreach(@keynext) {
                                                        $komut = `echo '($currentfen) , \e[1;29m$_\e[0m = \e[1;32m$nextmovespastvalues{$_}\e[0m' >> updated_list`;
                                                        }
                                    $komut = `echo '-------------------------------------' >> updated_list`;
                                    $komut = `echo '\e[1;34mMaxQnext = $max_v_next_state\e[0m' >> updated_list`;
                                    $komut = `echo '\e[1;31mCalculating...:\e[0m\e[1;29m (1-$alfa)*$oldq+$alfa*($reward+$beta*$max_v_next_state) = $newq\e[0m' >> updated_list`;
                                    $komut = `echo '\e[1;29mNewQ\e[0m = \e[1;44m$newq\e[0m' >> updated_list`;
                                    $komut = `echo '-------------------------------------' >> updated_list`;
                                    $komut = `echo '---> ($newline1) MAX: $max_v_next_state' >> updated_list`;
                                    $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list`;


                                    $updatequery = "update qtable set value='$newq' where fen='$lastfenlist2[-1]' and move='$whitemovelist[-1]'";
                                    $adh = $dbh->prepare($updatequery);
                                    $adh->execute or print "Can't update!\n";

                                    $readquery = "select value from qtable where fen='$lastfenlist2[-1]' and move='$whitemovelist[-1]'";
                                    $rdh = $dbh->prepare($readquery);
                                    $rdh->execute or print "Can't read!\n";
                                    $a = $rdh->fetchrow;
                                    print "D: $a\n";


                                    $search = "$lastfenlist2[-1]:$whitemovelist[-1]";
                                    my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                    print "kayitin oyun sira indexi: $index\n";

                                    if ($index > 0) {
                                    print "Ayrica asagidakiler guncellenecektir:\n";


                                    for($i=$index-1;$i>=0;$i--) {
                                                                $rewardback = 0;
                                                                print "$seq[$i]\n-------------\n";
                                                                print "\nNext State : ".($seq[$i+1])."\n";
                                                                @thisarr = split(/:/,$seq[$i]);
                                                                @nextarr = split(/:/,$seq[$i+1]);
                                                                print "Nextfen : $nextarr[0]\n";

                                                                $maxvquery = "select fen,move,value from qtable where fen='$nextarr[0]' order by value desc limit 1";
                                                                $mdh = $dbh->prepare($maxvquery);
                                                                $mdh->execute or print "Can't read!\n";
                                                                @a = $mdh->fetchrow_array();

                                                                print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                $max_v_next_state1 = $a[2];
                                                                $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                $updatequery = "update qtable set value='$newq' where fen='$thisarr[0]' and move='$thisarr[1]'";
                                                                $udh = $dbh->prepare($updatequery);
                                                                $udh->execute or print "Can't update!\n";

                                                                }
                                                    }


                                        }

                elsif ($mate eq 'draw') {

                                    print "Updating record ($lastfenlist2[-1]:$whitemovelist[-1])...(DRAW)\n";#$dur = <STDIN>;
                                    $newq = (1-$alfa)*$q{$lastfenlist2[-1]}{$whitemovelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                                    print "OLD: $oldq\nNEW: $newq\n";
                                    $q{$lastfenlist2[-1]}{$whitemovelist[-1]} = $newq;

                                    $oldline = "$lastfenlist2[-1]:$whitemovelist[-1]:$oldq";
                                    $oldline1 = $oldline;
                                    $newline = "$lastfenlist2[-1]:$whitemovelist[-1]:$newq";
                                    $newline1 = $newline;

                                    $oldline =~ s/\//\\\//g;
                                    $newline =~ s/\//\\\//g;

                                    $komut1 = `mv qtable.db tempdb.db`;
                                    $sed = `sed 's/$oldline/$newline/g' tempdb.db > qtable.db`;

#                                    $komut = `echo 'Updating ($oldline) ---> ($newline) MAX: $max_v_next_state' >> updated_list`;
                                    $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list`;
#                                   $komut = `echo '========================================================================================================' >> updated_list`;
                                    $komut = `echo 'Updating ($oldline1) (DRAW)...' >> updated_list`;
                                    $komut = `echo '\e[1;29mOldQ\e[0m = \e[1;41m$oldq\e[0m' >> updated_list`;
                                    $komut = `echo '-------------------------------------' >> updated_list`;
                                    $komut = `echo 'Previously visited (NextState,VisitedMoves) :' >> updated_list`;
                                    $komut = `echo '-------------------------------------' >> updated_list`;
                                    foreach(@keynext) {
                                                        $komut = `echo '($currentfen) , \e[1;29m$_\e[0m = \e[1;32m$nextmovespastvalues{$_}\e[0m' >> updated_list`;
                                                        }
                                    $komut = `echo '-------------------------------------' >> updated_list`;
                                    $komut = `echo '\e[1;34mMaxQnext = $max_v_next_state\e[0m' >> updated_list`;
                                    $komut = `echo '\e[1;31mCalculating...:\e[0m\e[1;29m (1-$alfa)*$oldq+$alfa*($reward+$beta*$max_v_next_state) = $newq\e[0m' >> updated_list`;
                                    $komut = `echo '\e[1;29mNewQ\e[0m = \e[1;44m$newq\e[0m' >> updated_list`;
                                    $komut = `echo '-------------------------------------' >> updated_list`;
                                    $komut = `echo '---> ($newline1) MAX: $max_v_next_state' >> updated_list`;
                                    $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list`;


                                    $updatequery = "update qtable set value='$newq' where fen='$lastfenlist2[-1]' and move='$whitemovelist[-1]'";
                                    $adh = $dbh->prepare($updatequery);
                                    $adh->execute or print "Can't update!\n";

                                    $readquery = "select value from qtable where fen='$lastfenlist2[-1]' and move='$whitemovelist[-1]'";
                                    $rdh = $dbh->prepare($readquery);
                                    $rdh->execute or print "Can't read!\n";
                                    $a = $rdh->fetchrow;
                                    print "E: $a\n";

                                    $search = "$lastfenlist2[-1]:$whitemovelist[-1]";
                                    my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                    print "kayitin oyun sira indexi: $index\n";

                                    if ($index > 0) {
                                    print "Ayrica asagidakiler guncellenecektir:\n";


                                    for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                                print "$seq[$i]\n-------------\n";
                                                                print "\nNext State : ".($seq[$i+1])."\n";
                                                                @thisarr = split(/:/,$seq[$i]);
                                                                @nextarr = split(/:/,$seq[$i+1]);
                                                                print "Nextfen : $nextarr[0]\n";

                                                                $maxvquery = "select fen,move,value from qtable where fen='$nextarr[0]' order by value desc limit 1";
                                                                $mdh = $dbh->prepare($maxvquery);
                                                                $mdh->execute or print "Can't read!\n";
                                                                @a = $mdh->fetchrow_array();

                                                                print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                $max_v_next_state1 = $a[2];
                                                                $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                $updatequery = "update qtable set value='$newq' where fen='$thisarr[0]' and move='$thisarr[1]'";
                                                                $udh = $dbh->prepare($updatequery);
                                                                $udh->execute or print "Can't update!\n";

                                                                }
                                                    }

                                        }

                else {
                        print "Updating record ($lastfenlist2[-1]:$whitemovelist[-1])...\n";#$dur = <STDIN>;
                        # QTABLE HESABI burada yapiliyor
                        $newq = (1-$alfa)*$q{$lastfenlist2[-1]}{$whitemovelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                        $q{$lastfenlist2[-1]}{$whitemovelist[-1]} = $newq;
#                                   $q{$lastfenlist2[-1]}{$whitemovelist[-1]} = (1-$alfa)*$q{$lastfenlist2[-1]}{$whitemovelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                        #print "$lastfenlist2[-1]:$whitemovelist[-1]  --> kayit guncellendi.\n";
                        #print "Old: $oldq, New: $q{$lastfenlist2[-1]}{$whitemovelist[-1]}";
                        $oldline = "$lastfenlist2[-1]:$whitemovelist[-1]:$oldq";
                        $oldline1 = $oldline;
                        $newline = "$lastfenlist2[-1]:$whitemovelist[-1]:$newq";
                        $newline1 = $newline;

                        $oldline =~ s/\//\\\//g;
                        $newline =~ s/\//\\\//g;

                        $komut1 = `mv qtable.db tempdb.db`;
                        $sed = `sed 's/$oldline/$newline/g' tempdb.db > qtable.db`;

                        $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list`;
#                        $komut = `echo '========================================================================================================' >> updated_list`;
                        $komut = `echo 'Updating ($oldline1)...' >> updated_list`;
                        $komut = `echo '\e[1;29mOldQ\e[0m = \e[1;41m$oldq\e[0m' >> updated_list`;
                        $komut = `echo '-------------------------------------' >> updated_list`;
                        $komut = `echo 'Previously visited (NextState,VisitedMoves) :' >> updated_list`;
                        $komut = `echo '-------------------------------------' >> updated_list`;
                        foreach(@keynext) {
                                            $komut = `echo '($currentfen) , \e[1;29m$_\e[0m = \e[1;32m$nextmovespastvalues{$_}\e[0m' >> updated_list`;
                                            }
                        $komut = `echo '-------------------------------------' >> updated_list`;
                        $komut = `echo '\e[1;34mMaxQnext = $max_v_next_state\e[0m' >> updated_list`;
                        $komut = `echo '\e[1;31mCalculating...:\e[0m\e[1;29m (1-$alfa)*$oldq+$alfa*($reward+$beta*$max_v_next_state) = $newq\e[0m' >> updated_list`;
                        $komut = `echo '\e[1;29mNewQ\e[0m = \e[1;44m$newq\e[0m' >> updated_list`;
                        $komut = `echo '-------------------------------------' >> updated_list`;
                        $komut = `echo '---> ($newline1) MAX: $max_v_next_state' >> updated_list`;
                        $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list`;

                        $updatequery = "update qtable set value='$newq' where fen='$lastfenlist2[-1]' and move='$whitemovelist[-1]'";
                        $adh = $dbh->prepare($updatequery);
                        $adh->execute or print "Can't update!\n";

                        $readquery = "select value from qtable where fen='$lastfenlist2[-1]' and move='$whitemovelist[-1]'";
                        $rdh = $dbh->prepare($readquery);
                        $rdh->execute or print "Can't read!\n";
                        $a = $rdh->fetchrow;
                        print "F: $a\n";

                        $search = "$lastfenlist2[-1]:$whitemovelist[-1]";
                        my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                        print "kayitin oyun sira indexi: $index\n";

                        if ($index > 0) {
                        print "\nAyrica asagidakiler guncellenecektir:\n";


                        for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                    print "\n$seq[$i]\n-------------\n";
                                                    print "\nNext State : ".($seq[$i+1])."\n";
                                                    @thisarr = split(/:/,$seq[$i]);
                                                    @nextarr = split(/:/,$seq[$i+1]);
                                                    print "Nextfen : $nextarr[0]\n";

                                                    $maxvquery = "select fen,move,value from qtable where fen='$nextarr[0]' order by value desc limit 1";
                                                    $mdh = $dbh->prepare($maxvquery);
                                                    $mdh->execute or print "Can't read!\n";
                                                    @a = $mdh->fetchrow_array();

                                                    print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                    $max_v_next_state1 = $a[2];
                                                    $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                    print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                    $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                    $updatequery = "update qtable set value='$newq' where fen='$thisarr[0]' and move='$thisarr[1]'";
                                                    $udh = $dbh->prepare($updatequery);
                                                    $udh->execute or print "Can't update!\n";

                                                    }
                                            }

#                        $komut = `echo '========================================================================================================' >> updated_list`;

                        #$komut = `echo 'Updating ($oldline) ---> ($newline) MAX: $max_v_next_state' >> updated_list`;
                        }


        push(@qtable,$newq);

            }
        }

    }
########################### END qtable() ############################################










################### thirdrepetition($t) #######################################
sub thirdrepetition { #thirdrepetition($t)

$thirdrep = 0;
my($t)  = @_;

if ( $t eq 'w' ) {
                    $thirdturn = 'blackmovelist';
                    $thirdfen = 'fenlistblack';
                    }
else {$thirdturn = 'whitemovelist';$thirdfen = 'fenlistwhite';};

if ( @{$thirdturn}[-1] eq @{$thirdturn}[-3] and @{$thirdturn}[-3] eq @{$thirdturn}[-5] ) {
                                                                                            $thirdrep = 1;
                                                                                            if ($t eq 'w') {print "\e[1;5;33mTHIRD REPETITION (B)!\e[0m\n\n";}
                                                                                            else {print "\e[1;5;33mTHIRD REPETITION (W)!\e[0m\n\n";}
                                                                                            }
# thirdrepetition() calismaz duruma gelir.
#$thirdrep = 0;
return $thirdrep;
                   }
################### END thirdrepetition($t) ###################################







################### update_database($turn,$move) ##############################
sub update_database {

my($ut,$umove)  = @_;

if ($countmove > 2) {
                    open(SAVEDLEARNING,"+>> savedlearnings_database.db");
                    print SAVEDLEARNING "$lastfenlist[-2]:$whitemovelist[-2]:$qtable[-2]\n";
                    close(SAVEDLEARNING);
$gamelist .= $whitemovelist[-2];
$gamelist .= " => Q[".(($countmove-2)/2)."]=".($qtable[($countmove)/2]);
$gamelist .= ":";
                    }
            }
################### END update_database($turn,$move) ##########################










################### state in puan olarak degerlendirmesinin yapilmasi. #########################
################################################################################################
# pozisyon degerlendirmesi delta ef icin
sub state_evaluate { #state_evaluate($st,$stable)

my($st,$stable) = @_;
if ($stable eq '') {$stable = 'board';}



########################### BOARD CONTROL,MATERIAL BALANCE,ROOKON7 and KING DISTANCE ###################################
$bcontrol = 0;
$mat_bal = 0;
$rookon7 = 0;
$kingdistance = 0;
$connected = 0;
$state_evaluate_puan = 0;

foreach $h (a..h) {
                    foreach $s (1..8) {
                                        if ( ${$stable}{$h}[$s] ne '-' ) {
                                                                            ##### BOARD CONTROL BLOCK ######################
                                                                            if ( (split(//,${$stable}{$h}[$s]))[0] eq 'w' ) {
                                                                                                                                $bcontrol += length(posmoves('w',$h,$s,$stable))/40;
                                                                                                                                }
                                                                            else {
                                                                                    $bcontrol -= length(posmoves('b',$h,$s,$stable))/40;
                                                                                    }
                                                                                    #}
                                                                            #$bcontrol = $bcontrol/10;
                                                                            #$bcontrol = $bcontrol/10;
                                                                            #$bcontrol = int ceil($bcontrol);
                                                                            ############## END BOARD CONTROL BLOCK ##########



                                                                            ###### MAT BALANCE BLOCK ########################
                                                                            if ( (split(//,${$stable}{$h}[$s]))[0] eq $st ) {
                                                                                                                            if ( (split(//,${$stable}{$h}[$s]))[1] eq 'K' ) {$mat_bal += 100;}
                                                                                                                            elsif ( (split(//,${$stable}{$h}[$s]))[1] eq 'R' ) {$mat_bal += 5;}
                                                                                                                            }
                                                                            else {
                                                                                    if ( (split(//,${$stable}{$h}[$s]))[1] eq 'K' ) {$mat_bal -= 100;}
                                                                                    elsif ( (split(//,${$stable}{$h}[$s]))[1] eq 'R' ) {$mat_bal -= 5;}
                                                                                    }
                                                                            ################## END MAT BALANCE BLOCK #########



                                                                            ############## ROOK ON 7th LINE BLOCK ######################
                                                                            if ($s == 7) {
                                                                                            if ( ${$stable}{$h}[7] =~ m/wR/ ) {if ($rookon7 == 0) {$rookon7 = $rookon7point;}}
                                                                                            }
                                                                            ############### END ROOK ON 7 ###############################



                                                                            ############################# KING DISTANCE from CENTER #############################
                                                                            if (${$stable}{$h}[$s] eq 'wK') {
                                                                                                            if (($s == 4 or $s == 5) and $h =~ m/[de]/ ) {
                                                                                                                                                            $kingdistance += $kingdistance1point;
                                                                                                                                                            }
                                                                                                            elsif (($s >= 3 or $s <= 6) and $h =~ m/[cdef]/ ) {
                                                                                                                                                                $kingdistance += $kingdistance2point;
                                                                                                                                                                }
                                                                                                            elsif (($s >= 2 or $s <= 7) and $h =~ m/[bcdefg]/ ) {
                                                                                                                                                                $kingdistance += $kingdistance3point;
                                                                                                                                                                }
                                                                                                            elsif (($s >= 1 or $s <= 8) and $h =~ /m[abcdefgh]/ ) {
                                                                                                                                                                    $kingdistance += $kingdistance4point;
                                                                                                                                                                    }
                                                                                                            }
                                                                            ################################# END kingdistance ##################################################
                                                                        }
                                        }
                    }
#print "Board control : $bcontrol\n";
################## END BOARD CONTROL ########################################





##############  CONNECTED ROOKS ##############################
undef(%evalpos);
%evalpos = getpieces($stable);
$pr1 = $evalpos{'wR1'};
$pr2 = $evalpos{'wR2'};
$rk = 0;

if ( $pr1 ne '' and $pr2 ne '' ) {
                                    if ( (split(//,$pr1))[0] eq (split(//,$pr2))[0] ) { #SUTUNDAKI ROOK LAR
                                                                                        undef(@s);
                                                                                        $x1 = (split(//,$pr1))[1];
                                                                                        $x2 = (split(//,$pr2))[1];
                                                                                        push(@s,$x1);push(@s,$x2);
                                                                                        @s = sort(@s);$rk=0;

                                                                                        foreach($s[0]..$s[1]) {
                                                                                                                $tara = (split(//,${$stable}{(split(//,$pr1))[0]}[$_]))[0].(split(//,${$stable}{(split(//,$pr1))[0]}[$_]))[1];
                                                                                                                if ( ${$stable}{(split(//,$pr1))[0]}[$_] ne '-' and $tara ne 'wR' ) {print "aha\n";$rk=1;}
                                                                                                                }

                                                                                                                if ( $rk < 1 ) {
                                                                                                                                $connected = $connectedpoint;
                                                                                                                                }
                                                                                        }

                                    elsif ( (split(//,$pr1))[1] eq (split(//,$pr2))[1] ) { #SATIRDAKI ROOK LAR
                                                                                            undef(@s);
                                                                                            $x1 = (split(//,$pr1))[0];
                                                                                            $x2 = (split(//,$pr2))[0];
  #                                                                                          print "X1: $x1, X2: $x2\n";
 #                                                                                           print "pr1: $pr1, pr2: $pr2\n";
                                                                                            push(@s,$x1);push(@s,$x2);
                                                                                            @s = sort(@s);$rk=0;
#                                                                                            print "s0 = $s[0], s1 = $s[1]\n";

                                                                                            foreach($s[0]..$s[1]) {
                                                                                                                    $tara = (split(//,${$stable}{$_}[(split(//,$pr1))[1]]))[0].(split(//,${$stable}{$_}[(split(//,$pr1))[1]]))[1];
                                                                                                                    if ( ${$stable}{$_}[(split(//,$pr1))[1]] ne '-' and $tara ne 'wR' ) {print "eho\n";$rk=1;}
                                                                                                                    }

                                                                                                                    if ( $rk < 1 ) {
                                                                                                                                    $connected = $connectedpoint;
                                                                                                                                    }
                                                                                            }
                                    }
############### END CONNECTED ROOKS ##########################

print "BC=$bcontrol, MB=$mat_bal, R7=$rookon7, KD=$kingdistance, CON=$connected\n";

$state_evaluate_puan = $bcontrol+$mat_bal+$rookon7+$kingdistance+$connected;

return $state_evaluate_puan;

                    }
######################################## END of state_evaluate($st,$stable) #########################










######### herhangi bir tahtadaki tum taslarin koordinatlarini bir hash diziye veren fonksiyon.
sub getpieces { #getpieces($ptable)

undef(%gpieces);%gpieces = ();

my($ptable)  = @_;

if ($ptable eq '') {$ptable = 'board';}

foreach $h (a..h) {
                foreach $s (1..8) {
                            if (${$ptable}{$h}[$s] ne '-') {$gpieces{${$ptable}{$h}[$s]} = $h.$s;}
                            }
                    }
return %gpieces;
                }
######## getpieces($ptable) sonu ############################################################






################# giristeki komut listesini yazar ###########################################
sub commandlist { #commandlist();
print "\e[0;0;4mMove\e[0m        -> Enter move manually (ex. \e[1;31mf7h7\e[0m).\n";
print "\e[0;0;4mAuto\e[0m        -> Enter '\e[1;31ma\e[0m' or '\e[1;31mauto\e[0m' to pick a move from nalimov t.b.\n";
print "\e[0;0;4mAuto(learn)\e[0m -> Enter '\e[1;31mal\e[0m','\e[1;31malearn\e[0m','\e[1;31mautolearn\e[0m' to pick a move from nalimov t.b.\n";
print "\e[0;0;4mGaming\e[0m      -> Enter '\e[1;31mgm\e[0m','\e[1;31mg\e[0m' or '\e[1;31mgamemode\e[0m' to pick move from learned db.\n";
print "\e[0;0;4mLearning\e[0m    -> Enter '\e[1;31mlm\e[0m' or '\e[1;31mlearnmode\e[0m' for learning mode (writes to qtable.db).\n";
                }
################ end commandlist() ##########################################################







##################### learning() ############################################################
sub learning {

%allqmoves = getqmoves($turn);

@keys = (sort hashValueAscendingNum (keys(%allqmoves)));

$size=scalar(keys %allqmoves);

$lsize = ($size-$size%5)*4/5;

undef(@lsize_moves);
undef(@usize_moves);

foreach (0..($lsize-1)) {
                        push(@lsize_moves,@keys[$_]);
                        }
foreach (($lsize)..($size-1)) {
                                push(@usize_moves,$keys[$_]);
                                }

if (@lsize_moves > 0) {
                        $random = int rand(10);

                        if ($random >= 0 ) {$secimliste = 'usize_moves';}
                        else {$secimliste = 'lsize_moves';};
                        }

else {$secimliste = 'usize_moves';};

# secimliste yi keys olarak atadigimizda tamamen random move secilir. Bu satir # lenirse yukaridaki move derecelendirme yapilir.
$secimliste = 'keys';

$pickmove = ${$secimliste}[rand @{$secimliste}];

print "MY PICKED MOVE: \e[0;0;44m$pickmove\e[0m ($q{$lastfenlist2[-1]}{$pickmove})\n";

if ($countmove < 1 ) {qtable($checkmate,'');}

$lastfen = fen('w','board');
push(@lastfenlist,$lastfen);

domove($pickmove,$turn);
            }
#################### END learning()##########################################################






###################### gaming() #############################################################
sub gaming {

$currentfen = fen('w','board');

if (!(-e "qtable.db")) {print "\nDatabase bulunamadı!\n\n";}
else {@aranan = `grep '$currentfen' qtable.db`;}

print fen('w','board')."\n";
print "BULUNANLAR : ".(scalar(@aranan))."\n";

if (scalar(@aranan) > 0) {
                        undef(@gamemodemoves);
                        # bulundugum durumda daha onceden oynanmis move listesi ve qtable degerleri.
                        %gamemodemoves = ();
                        foreach (@aranan) {
                                            if ($_ =~ m/$currentfen/ ) {
                                                                        @savedline = split(/:/,$_);
                                                                        $gamemodemoves{@savedline[1]} = @savedline[2];
                                                                        }

                                            }

                        @gamemodekeys = (sort hashValueAscendingNum1 (keys(%gamemodemoves)));
                        $gamingmove = @gamemodekeys[-1];
                        print "ALL MOVES PREVIOUS: \n";
                        foreach(@gamemodekeys) {print "$_ = $gamemodemoves{$_}\n";}
                        print "GAMING MODE MOVE : $gamingmove\n";
                        chomp($dur = <STDIN>);
                        domove($gamingmove,'w');
                        }
else {learning();};
}
############################## END gaming() ######################################################






############################## moveselect() #######################################################
sub moveselect { #moveselect('learn');

my($lr) = @_;

#print "LR: $lr, TURN: $turn\n";
showtable();
###################### PICKING THE TURN AND GETTING THE MOVE ##############################################
#### oyunun basindaki baslangic bolumunde girilecek komut ya da elle move girme.
@gm = games($turn,0);

#if ( (games($turn,0))[1] < 3 or thirdrepetition($turn) > 0 or  ) {draw();}
#if ( @gm[1] < 3 or thirdrepetition($turn) > 0 or (length(@gm[0])<1 and $checkmated < 1 ) ) {draw();}
if ( @gm[1] < 3 or (length(@gm[0])<1 and $checkmated < 1 ) ) {draw();}

elsif ($turn eq "w") {
                        if ( (games($turn,0))[0] ne '' ) {
                                                                if (scalar(@seq) > 0) {
                                                                                        print "Move sayımız: ".(scalar(@seq))."\n";
                                                                                        foreach(@seq) {
                                                                                                        #print "$_[0],$_[1]\n";
                                                                                                        print "$_\n";
                                                                                                        }
                                                                                        print "------------------\n";
                                                                                        };

                                                                #print "A : $seq[-1][0]\n";
                                                                push(@sp,state_evaluate($turn));

                                                                #auto mode ogrenme icin asagidaki satir acilir
                                                                if ($lr eq 'learn') {qtable($checkmate,'');};


                                                                if($countmove > 1) {
#                                                                                    $whitemovepoints{$whitemovelist[-1]} = $sp[-1]-$sp[-2];
                                                                                    $whitemovepoints{$whitemovelist[-1]} = $sp[-1]-$sp[-2]+$checkmate;
                                                                                    }

                                                                if ($countmove > 1) {print "Checkmate\t\t: $checkmate\n";print "Prev. state\t\t: $sp[-2]\n";}
                                                                print "This state\t\t: $sp[-1]\n";
                                                                if ($countmove > 1) {
                                                                                    if ($whitemovepoints{$whitemovelist[-1]} < 0 ) {
                                                                                                                                    print "Move (\e[0;0;44m$whitemovelist[-1]\e[0m) point\t: \e[0;1;31m$whitemovepoints{$whitemovelist[-1]}\e[0m\n\n";
                                                                                                                                    }

                                                                                    else {
                                                                                            print "Move (\e[0;0;44m$whitemovelist[-1]\e[0m) point\t: \e[0;1;32m$whitemovepoints{$whitemovelist[-1]}\e[0m\n\n";
                                                                                            }
                                                                                    }

                                                                if ($move eq '' and $gamemode == 1) {
                                                                                                    gaming();
                                                                                                    moveselect();
                                                                                                    };
                                                                if ($learnmode == 1) {
                                                                                        use Term::ReadKey;

                                                                                        while ( 1 ) {
                                                                                                    ReadMode( 'cbreak' );
                                                                                                    if ( defined ( my $key = ReadKey( -1 ) ) ) {
                                                                                                                                                if ($key eq 'q') {print "Learnmode stopped!\n";$learnmode = 0;moveselect();};
                                                                                                                                                }
                                                                                                                                                                                                                                                                        qtable($checkmate,'');
                                                                                                    update_database($turn,$move);
                                                                                                    learning();
                                                                                                    moveselect();

                                                                                                    ReadMode( 'normal' );
                                                                                                    }


                                                                                    };

                                                                if ($alearnmode == 1) {
                                                                                        qtable($checkmate,'');
                                                                                        update_database($turn,$move);
                                                                                        $alearnmode = 0;
                                                                                        };

                                                                if ($manual == 1) {
                                                                                    qtable($checkmate,'');
                                                                                    $manual = 0;
                                                                                    }


                                                                if ($countmove < 1) {commandlist();print "\n\e[1;29mWhite\e[0m to move (or command) : ";}
                                                                else {print "\e[1;29mWhite\e[0m to move (or command) : ";};
                                                                $move = $mateseq[$mi];$mi++;
                                                                #chomp($amove = <STDIN>);
                                                            }
                        else {mate();}
                    }

	else {
		if ( (games($turn,0))[0] ne '' ) {
						print "\e[1;31mBlack\e[0m ( Nalimov tablebase automove )  to move : ";
						automove($turn);moveselect();
                                        }
		else {mate();}
	      }


if ( ($move eq 'automove' or $move eq 'auto' or $move eq 'a') ) {automove($turn);moveselect();}
elsif ( ($move eq 'autolearn' or $move eq 'alearn' or $move eq 'al') ) {$alearnmode = 1;automove($turn);moveselect();}
elsif ( $move eq 'learnmode' or $move eq 'lm' or $learnmode == 1 ) {$learnmode = 1;learning();moveselect();}
elsif ( $move eq 'gamemode' or $move eq 'gm' or $move eq 'g' or $gamemode == 1 ) {$gamemode = 1;gaming();moveselect();}
##########################  END PICKING MOVE OR COMMAND ####################################################################

domove($move,$turn);
$manual = 1;
legalmove();
                }
################################ END moveselect() ###########################################################################







################################# mate() ###################################################################################
sub mate {
if ($turn eq 'w') {
                    print "\e[1;29mWhite\e[0m";
                    }
else {print "\e[1;31mBlack\e[0m";};
print " (($countmove-($countmove%2))/2) was \e[1;5;35mMATE!\e[0m\n\n";

if ($learnmode == 1) {
                        open(GAMELIST,"+>> gamelist_saved.list");
                        print GAMELIST "$countmove->$gamelist\mate->$turn\n";
                        close(GAMELIST);
                open(SAVEDLEARNING,"+>> savedlearnings_database.db");
                print SAVEDLEARNING "$lastfen:$whitemove:$qtable[-1]:mate->$turn\n";
                close(SAVEDLEARNING);

                    }


if ($turn eq 'w'){
                    $reward = -$rewardpoint;
                    $splast = state_evaluate('w');
                    }
elsif ($turn eq 'b') {
                        $reward = $rewardpoint;
                        $splast = state_evaluate('w');
                        }
push(@sp,$splast);
$whitemovepoints{$whitemovelist[-1]} = $sp[-1]-$sp[-2]+$checkmate;

if ($countmove > 1) {print "Checkmate\t\t: $checkmate\n";print "Prev. state\t\t: $sp[-2]\n";}
print "This state\t\t: $sp[-1]\n";

if ($whitemovepoints{$whitemovelist[-1]} < 0 ) {
                                                print "Move (\e[0;0;44m$whitemovelist[-1]\e[0m) point\t: \e[0;1;31m$whitemovepoints{$whitemovelist[-1]}\e[0m\n\n";
                                                }

else {
        print "Move (\e[0;0;44m$whitemovelist[-1]\e[0m) point\t: \e[0;1;32m$whitemovepoints{$whitemovelist[-1]}\e[0m\n\n";
        }

#print "Move (\e[0;0;44m$whitemovelist[-1]\e[0m) point: \e[1;29m$whitemovepoints{$whitemovelist[-1]}\e[0m\n\n";
qtable($checkmate,'mate');$gamelist .= $qtable[-1].":";

startposition();
moveselect();
}
############################################################################################################################






################################# draw() ####################################################################################
sub draw {

if ($learnmode == 1) {
                        open(GAMELIST,"+>> gamelist_saved.list");
                        print GAMELIST "$countmove->$gamelist\draw\n";
                        close(GAMELIST);

                open(SAVEDLEARNING,"+>> savedlearnings_database.db");
                print SAVEDLEARNING "$lastfen:$whitemove:$qtable[-1]:draw\n";
                close(SAVEDLEARNING);
                        }

print "GAME was \e[1;5;36mDRAW!\e[0m\n\n";

$splast = state_evaluate('w');
push(@sp,$splast);
$reward = 0;
#$whitemovepoints{$whitemovelist[-1]} = 0;
$whitemovepoints{$whitemovelist[-1]} = $sp[-1]-$sp[-2]+$checkmate;

if ($countmove > 1) {print "Checkmate\t\t: $checkmate\n";print "Prev. state\t\t: $sp[-2]\n";}
print "This state\t\t: $sp[-1]\n";

if ($whitemovepoints{$whitemovelist[-1]} < 0 ) {
                                                print "Move (\e[0;0;44m$whitemovelist[-1]\e[0m) point\t: \e[0;1;31m$whitemovepoints{$whitemovelist[-1]}\e[0m\n\n";
                                                }

else {
        print "Move (\e[0;0;44m$whitemovelist[-1]\e[0m) point\t: \e[0;1;32m$whitemovepoints{$whitemovelist[-1]}\e[0m\n\n";
        }

#print "Move (\e[0;0;44m$whitemovelist[-1]\e[0m) point: \e[1;29m$whitemovepoints{$whitemovelist[-1]}\e[0m\n\n";

qtable($checkmate,'draw');$gamelist .= $qtable[-1].":";

startposition();
moveselect()
}
################################# END draw() ################################################################################








################################## legalmove() ####################################################
sub legalmove {
my($lmlr) = @_;
if ($turn eq 'b') {
                    print "\n\n\e[1;29mMoving...\e[0m : \e[0;0;44m$move\e[0m\n";
                    }
else {
        print "\n\n\e[1;29mMoving...\e[0m : \e[0;0;41m$move\e[0m\n";
        }
if ($lmlr eq 'learn') {moveselect('learn');}
else {moveselect();}
}
###################################################################################################






################################## legalmove() ####################################################
sub illegalmove {
print "\n\e[1;31mIllegal move!!!\e[0m : $move\n";
moveselect();
}
###################################################################################################







# hash yapilarda siralama icin gereken alt fonksiyonlar
##################################################################################
sub hashValueAscendingNum {
    $allqmoves{$a} <=> $allqmoves{$b};
}

sub hashValueDescendingNum {
    $allqmoves{$b} <=> $allqmoves{$a};
}
##################################################################################

##################################################################################
sub hashValueAscendingNum1 {
    $gamemodemoves{$a} <=> $gamemodemoves{$b};
}

sub hashValueDescendingNum1 {
    $gamemodemoves{$b} <=> $gamemodemoves{$a};
}
##################################################################################

##################################################################################
sub hashValueAscendingNum2 {
    $nextmovespastvalues{$a} <=> $nextmovespastvalues{$b};
}

sub hashValueDescendingNum2 {
    $nextmovespastvalues{$b} <=> $nextmovespastvalues{$a};
}
##################################################################################




end:
