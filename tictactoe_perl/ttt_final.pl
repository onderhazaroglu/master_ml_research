#!/usr/bin/perl

use POSIX;
use List::MoreUtils;
#use warnings;

############################## PROGRAM CONSTANTS #####################################
$learnmode = 0;
$alearnmode = 0;
$alfa = 0.5;
$beta = 0.8;
$rewardpoint = 200;
$newmethod = '0';
#$reward = 0;
$gamesayisi = 0;
######################################################################################


######################################################################################
use DBI;
my $database = 'gokce';
my $user = 'gokce';
my $pass = 'qlearning';
my $dsn = 'DBI:mysql:gokce:localhost:3306';
my $dbh = DBI->connect($dsn,$user,$pass) or die "Can’t connect to the DB: $DBI::errstr\n";

$initquery = "select code,turn,level,value from ttt";
$ith = $dbh->prepare($initquery);
$ith->execute or print "Can't read!\n";
$rowcount = $ith->rows();
$init = $ith->fetchall_hashref('code');
print "\nSuccessfully loaded $rowcount lines from db.\n";
######################################################################################


######################################################################################
undef(%q);
use DBI;
my $database = 'gokce';
my $user = 'gokce';
my $pass = 'qlearning';
my $dsn = 'DBI:mysql:gokce:localhost:3306';
my $dbh = DBI->connect($dsn,$user,$pass) or die "Can’t connect to the DB: $DBI::errstr\n";

$query = "select code,move,value from qtable_ttt";
$sth = $dbh->prepare($query) or print "Veri alınamadı!\n";
$sth->execute;
$rv = $sth->rows;

$sttime = time();
while(@row = $sth->fetchrow_array()) {
                                        $q{$row[0]}{$row[1]} = $row[2];
                                        }
$fntime = time();
$time = $fntime-$sttime;
print "\nSuccessfully loaded $rv records in $time seconds.\n";
######################################################################################









startposition();
moveselect();









#########################################################  FONKSIYON TANIMLAMALARI #######################################################################







################################## startposition() #############################################################
sub startposition {

$turn = "w";
$gamelist = '';
undef(@qtable);
$countmove = 0;
$reward = 0;
undef(@sp);
$alearnmode = 0;
$playerxo = 'X';
undef(@whitenodelist);
undef(@lastcodelist);
undef(@seq);
#### eger ogrenmede sabit bi sequence oynanmasi isteniyorsa bu line uncomment edilir ve liste yazilir.
#@mateseq = ("100000000","100120000","101120200","121121200");
#@mateseq = ("f7a7","d4c5","c5b6","a7d7");
$mi = 0;
if (scalar(@mateseq) > 0 and $learnmode eq '1' and $q{'000000000'}{'100000000'} == -102.4) {print "$gamesayisi\n";$learnmode = 0;}
$gamesayisi++;
undef(@board);
@board = (  ['1','2','3'],
            ['4','5','6'],
            ['7','8','9']   );
                    }
######################## END startposition() #######################################################





############################    showtable() sub function  #####################################
sub showtable() {
print "\n\n\n";
# Top string set a .. h
print "\t ------- ------- -------\n";
print "\t|       |       |       |\n";
for ($i=0;$i<3;$i++) {
                        for($j=0;$j<3;$j++) {
                                            if ($board[$i][$j] ne 'X' and $board[$i][$j] ne 'O') {print "\t|   \e[0;1;30m".(3*$i+$j+1)."\e[0m";if ($j==2) {print "   |\n";}}
                                            elsif ($board[$i][$j] eq 'X') {print "\t|   \e[0;1;31mX\e[0m";if ($j==2) {print "   |\n";}}
                                            elsif ($board[$i][$j] eq 'O') {print "\t|   \e[0;1;29mO\e[0m";if ($j==2) {print "   |\n";}}
                                            }
                        if ($i < 2) {
                                    print "\t|       |       |       |\n";
                                    print "\t ------- ------- -------\n";
                                    print "\t|       |       |       |\n";
                                    }
                        }
print "\t|       |       |       |\n";
print "\t ------- ------- -------\n";
print "\n\n\n";
print "Table code: ".get_table_code();
print "\n\n";
			}
################################# END showtable() sub function #####################################






############################## moveselect() #######################################################
sub moveselect { #moveselect('learn');
#print "moveselecte geldiginde turn: $turn\n";
my($lr) = @_;

#print "LR: $lr, TURN: $turn\n";
showtable();
###################### PICKING THE TURN AND GETTING THE MOVE ##############################################
#### oyunun basindaki baslangic bolumunde girilecek komut ya da elle move girme.
@gm = checkboard();


$triple = $gm[1];
@gm = @{$gm[0]};

if (scalar(@gm) < 1 and $triple == 0) {
                                        draw();
                                        }
elsif ($triple == 1 or $triple == -1) {mate();}

elsif ($turn eq 'w') {
                        if ( scalar(@gm) > 0 ) {
                                                                if (scalar(@seq) > 0) {
                                                                                        print "Move sayımız: ".(scalar(@seq))."\n";
                                                                                        foreach(@seq) {
                                                                                                        print "$_\n";
                                                                                                        }
                                                                                        print "------------------\n";
                                                                                        };

                                                                if ($lr eq 'learn') {qtable();};

                                                                if ($mode eq 'gameagentvscomp') {
                                                                                            game_agent_vs_comp();
                                                                                            moveselect();
                                                                                            }
                                                                elsif ($mode eq 'gameplayervsagent') {
                                                                                            game_player_vs_agent();
                                                                                            moveselect();
                                                                                            }

                                                                elsif ($mode eq 'gameplayervscomp') {
                                                                                            game_player_vs_comp();
                                                                                            moveselect();
                                                                                            }
                                                                elsif ($mode eq 'gameplayervsplayer') {
                                                                                            game_player_vs_player();
                                                                                            moveselect();
                                                                                            };
                                                                if ($learnmode == 1) {
                                                                                        use Term::ReadKey;

                                                                                        while ( 1 ) {
                                                                                                    ReadMode( 'cbreak' );
                                                                                                    if ( defined ( my $key = ReadKey( -1 ) ) ) {
                                                                                                                                                if ($key eq 'q') {print "Learnmode stopped!\n";$learnmode = 0;moveselect();ReadMode 0;};
                                                                                                                                                }
                                                                                                    qtable('');
                                                                                                    learning_agent_vs_comp();
                                                                                                    moveselect();
                                                                                                    ReadMode( 'normal' );
                                                                                                    }
                                                                                    };

                                                                if ($countmove < 1) {commandlist();print "\n\e[1;29mCommand:\e[0m ";}
                                                                else {print "\n\e[1;29mCommand:\e[0m ";};
                                                                chomp($command = <STDIN>);
                                                            }
                        else {mate();}
                    }

	else {
                if ( scalar(@gm) > 0 ) {
                                        print "\e[1;29mWhite (O)\e[0m to move : ";
                                        if ($mode eq 'gameplayervsagent') {
                                                                            game_player_vs_agent();
                                                                            moveselect();
                                                                            }
                                        elsif ($mode eq 'gameplayervsplayer') {
                                                                                game_player_vs_player();
                                                                                moveselect();
                                                                                }

                                        else {
                                                automove($turn);moveselect();
                                                }
                                        }
                else {mate();}
            }
###### 1-> learning (Agent vs. Computer(minmax))  (without new method)
###### 2-> learning (Agent vs. Computer(minmax))  (with new method)
###### 3-> gaming   (Agent vs. Computer(minmax))
###### 4-> gaming   (Player vs Agent)
###### 5-> gaming   (Player vs. Computer(minmax))
###### 6-> gaming   (Player vs. Player)

if    ($command eq '1') {$newmethod = '0';$learnmode = 1;moveselect();}
elsif ($command eq '2') {$newmethod = '1';$learnmode = 1;moveselect();}
elsif ($command eq '3') {$learnmode = 0;$mode = 'gameagentvscomp';game_agent_vs_comp();moveselect();}
elsif ($command eq '4') {$learnmode = 0;$mode = 'gameplayervsagent';game_player_vs_agent();moveselect();}
elsif ($command eq '5') {$learnmode = 0;$mode = 'gameplayervscomp';game_player_vs_comp();moveselect();}
elsif ($command eq '6') {$learnmode = 0;$mode = 'gameplayervsplayer';game_player_vs_player();moveselect();}
else {print "Wrong command.\n";$command = '';moveselect();}
##########################  END PICKING MOVE OR COMMAND ####################################################################

                }
################################ END moveselect() ###########################################################################




##################### game_player_vs_comp() ############################################################
sub game_player_vs_comp {
if ($playerxo eq '') {setplayerxo();}
if ($turn eq 'w') {
                    print "Give a place to put $playerxo: ";
                    chomp($place = <STDIN>);
                    @emptyplaces = @{(checkboard())[0]};
                    $gamelistplace = $place.":";
                    $place = convertplace($place);
                    if ($place ne '' and grep {$_ eq $place} @emptyplaces) {
                                                                            $gamelist .= $gamelistplace;
                                                                            $board[(split(//,$place))[0]][(split(//,$place))[1]] = $playerxo;
                                                                            $countmove++;
                                                                            $turn = 'b';
                                                                            moveselect();
                                                                            }
                    else {print "Wrong place.\n";moveselect();break;}
                    }
else {$turn = 'b';moveselect();}
                }
#################### END game_player_vs_comp() #################################################




##################### learning_agent_vs_comp() ############################################################
sub learning_agent_vs_comp {
if ($turn eq 'w') {
                $nowtcode = get_table_code();
                push(@lastcodelist,$nowtcode);
                if (scalar(@mateseq) > 0) {$agentcode = $mateseq[$mi];$mi++;}
                else {
                        print "Agent's random move: ";
                        @agentmoves = get_sub_nodes2($nowtcode);
                        #print "\nPos. Move sayisi : ".scalar(@agentmoves)."\n";
                        $agentcode = $agentmoves[int rand(scalar(@agentmoves))];
                        }
                push(@whitenodelist,$agentcode);
                ##########################################
                $ekleseq = "$nowtcode:$agentcode";
                push(@seq,$ekleseq);
                ##########################################
                $place = compare_codes($agentcode,$nowtcode);
                $gamelist .= $place.":";
                print "\e[0;0;44m$place\e[0m\n";
                @emptyplaces = @{(checkboard())[0]};
                $place = convertplace($place);
                $board[(split(//,$place))[0]][(split(//,$place))[1]] = 'X';
                $countmove++;
                $turn = 'b';
                moveselect('learn');
                }
else {$turn = 'b';moveselect();}
                }
#################### END learning_agent_vs_comp()##########################################################



##################### game_agent_vs_comp() ############################################################
sub game_agent_vs_comp {
if ($turn eq 'w') {
                    @emptyplaces = @{(checkboard())[0]};
                    $nowtcode = get_table_code();

                    $maxcquery = "select move from qtable_ttt where code='$nowtcode' order by value desc limit 1";
                    $mxcdh = $dbh->prepare($maxcquery);
                    $mxcdh->execute or print "Can't read!\n";
                    $choice = $mxcdh->fetchrow();

                    if ($mxcdh->rows() > 0) {

                                                $query = "select move,value from qtable_ttt where code='$nowtcode' order by value desc";
                                                $qdh = $dbh->prepare($query);
                                                $qdh->execute or print "Can't read!\n";
                                                print "\nFounded moves: \n-----------------\n";
                                                while (@row = $qdh->fetchrow_array()) {
                                                                                        print "$row[0] (".compare_codes($row[0],$nowtcode).") : $row[1]\n";
                                                                                        }


                                                $place = compare_codes($choice,$nowtcode);
                                                }

                    else {
                            $choice = $emptyplaces[int rand(scalar(@emptyplaces))];
                            @cho = split(//,$choice);
                            $place = 3*$cho[0]+$cho[1]+1;
                            };


                    $gamelistplace = $place;
                    print "-----------------\nAgent's move: \e[0;0;44m$place\e[0m\n\n[Hit enter to continue]\n";$dur = <STDIN>;


                    $place = convertplace($place);
                    if ($place ne '' and grep {$_ eq $place} @emptyplaces) {
                                                                            $gamelist .= $gamelistplace.":";
                                                                            $board[(split(//,$place))[0]][(split(//,$place))[1]] = $playerxo;
                                                                            $countmove++;
                                                                            $turn = 'b';
                                                                            moveselect();
                                                                            }
                    else {print "Wrong place.\n";moveselect();break;}
                    }
else {$turn = 'b';moveselect();}
                }
#################### END game_agent_vs_comp() #################################################




##################### game_player_vs_agent() ############################################################
sub game_player_vs_agent {
if ($turn eq 'w') {
                    @emptyplaces = @{(checkboard())[0]};
                    $nowtcode = get_table_code();

                    $maxcquery = "select move from qtable_ttt where code='$nowtcode' order by value desc limit 1";
                    $mxcdh = $dbh->prepare($maxcquery);
                    $mxcdh->execute or print "Can't read!\n";
                    $choice = $mxcdh->fetchrow();

                    if ($mxcdh->rows() > 0) {

                    $query = "select move,value from qtable_ttt where code='$nowtcode' order by value desc";
                    $qdh = $dbh->prepare($query);
                    $qdh->execute or print "Can't read!\n";

                    print "\nFounded moves: \n-----------------\n";
                    while (@row = $qdh->fetchrow_array()) {
                                                            print "$row[0] (".compare_codes($row[0],$nowtcode).") : $row[1]\n";
                                                            }

                    $place = compare_codes($choice,$nowtcode);
                                        }
                    else {
                            $choice = $emptyplaces[int rand(scalar(@emptyplaces))];
                            @cho = split(//,$choice);
                            $place = 3*$cho[0]+$cho[1]+1;
                            };

                    print "-----------------\nAgent's move: \e[0;0;44m$place\e[0m\n\n[Hit enter to continue]\n";$dur = <STDIN>;

                    $gamelistplace = $place;

                    $place = convertplace($place);

                    if ($place ne '' and grep {$_ eq $place} @emptyplaces) {
                                                                            $gamelist .= $gamelistplace.":";
                                                                            $board[(split(//,$place))[0]][(split(//,$place))[1]] = $playerxo;
                                                                            $countmove++;
                                                                            $turn = 'b';
                                                                            moveselect();
                                                                            }
                    else {print "Wrong place.\n";moveselect();break;}
                    }
else {
        print "\nGive a place to put O: ";
        chomp($place = <STDIN>);
        $gamelistplace = $place;
        @emptyplaces = @{(checkboard())[0]};
        $place = convertplace($place);
        if ($place ne '' and grep {$_ eq $place} @emptyplaces) {
                                                                $gamelist .= $gamelistplace.":";
                                                                $board[(split(//,$place))[0]][(split(//,$place))[1]] = 'O';
                                                                $countmove++;
                                                                $turn = 'w';
                                                                moveselect();
                                                                }
        else {print "Wrong place.\n";moveselect();break;}
        }
                        }
#################### END game_player_vs_agent() #################################################




##################### game_player_vs_player() ############################################################
sub game_player_vs_player {
if ($turn eq 'w') {
                    print "\nGive a place to put X: ";
                    chomp($place = <STDIN>);
                    $gamelistplace = $place;
                    @emptyplaces = @{(checkboard())[0]};
                    $place = convertplace($place);
                    if ($place ne '' and grep {$_ eq $place} @emptyplaces) {
                                                                            $gamelist .= $gamelistplace.":";
                                                                            $board[(split(//,$place))[0]][(split(//,$place))[1]] = 'X';
                                                                            $countmove++;
                                                                            $turn = 'b';
                                                                            moveselect();
                                                                            }
                    else {print "Wrong place.\n";moveselect();break;}


                    }
else {
        print "\nGive a place to put O: ";
        chomp($place = <STDIN>);
        $gamelistplace = $place;
        @emptyplaces = @{(checkboard())[0]};
        $place = convertplace($place);
        if ($place ne '' and grep {$_ eq $place} @emptyplaces) {
                                                                $gamelist .= $gamelistplace.":";
                                                                $board[(split(//,$place))[0]][(split(//,$place))[1]] = 'O';
                                                                $countmove++;
                                                                $turn = 'w';
                                                                moveselect();
                                                                }
        else {print "Wrong place.\n";moveselect();break;}
        }
                        }
#################### END game_player_vs_agent() #################################################





############################ initialize() ##########################################################
sub initialize {
my($aturn) = @_;
if ($aturn eq '') {$aturn = 'X';}
undef(%hesapla);
@nodes = ('000000000');
$number_nodes = 0;
open(FILE,"+> tree.txt");
#$aturn = 'X';
while (scalar(@nodes) > 0 ) {
                                %allsubs = get_sub_nodes('nodes',$aturn);
                                undef(@nodes);
                                @keys = (keys %allsubs);
                                print "---------------------Sayisi: ".scalar(@keys)."\n";
                                #print "\n";
                                #$dur = <STDIN>;
                                foreach $key (@keys) {
                                                @values = @{$allsubs{$key}};
                                                $trf = get_triple($key);

                                                if ($trf eq '0') {$trf = 0;}
                                                elsif ($trf eq '1') {$trf = 1;}
                                                elsif ($trf eq '2') {$trf = -1;}

                                                print FILE "$key.$aturn=$trf->";
                                                #print "\n$key.$aturn=$trf->\n";
                                                #if ($trf == 0) {
                                                foreach $a (@values) {
                                                                        #print "$a,";
                                                                        push(@nodes,$a);
                                                                        $tr = get_triple($a);
                                                                        if ($tr eq '0') {$trp = 0;}
                                                                        elsif ($tr eq '1') {$trp = 1;}
                                                                        elsif ($tr eq '2') {$trp = -1;}
                                                                        if ($aturn eq 'X') {$aturn2 = 'O';} else {$aturn2 = 'X';};
                                                                        print FILE "$a.$aturn2=$trp:";
                                                                        #print "$a.$aturn2=$trp:";

                                                                        $general{$a}[0] = $trp;
                                                                        $general{$a}[1] = $aturn2;
                                                                        print "$a=$general{$a}[0],$general{$a}[1]\n";
                                                                        push(@{$hesapla{$key}},$a);
                                                                        $i++;

                                                                        }
                                                 #               }

                                                print FILE "\n";
                                                    }
                                $number_nodes += scalar(@nodes);
                                #print "Number of Elements : $number_nodes\n";
                                if ($aturn eq 'O') {$aturn = 'X';} else {$aturn = 'O';}
                                }
close(FILE);
#                            print "Hesaplama listesi: \n";
#                            @hkeys = (keys %hesapla);
#                            foreach (@hkeys) {
#                                                @hes = @{$hesapla{$_}};
#                                                print "$_-> ";
#                                                foreach $x (@hes) {print "$x,";}
#                                                print "\n";
#                                                }



#$vkey = '220100100';
#print "$vkey: ".get_value($vkey)."\n";
                    }
############################## END initialize() ##################################################################


##########################################
sub max {
foreach(@_) {push(@m,$_);};

@m = sort(@m);
return $m[-1];
        }
##########################################


##########################################
sub min {
foreach(@_) {push(@m,$_);};

@m = sort(@m);
return $m[0];
        }
##########################################

###############################################
sub get_dbvalue {
my($dbcode) = @_;

$readqdb = "select value from ttt where code='$vcode'";
$rdhdb = $dbh->prepare($readqdb);
$rdhdb->execute or print "Can't read!\n";
return $rdhdb->fetchrow;
}
###############################################

###############################################
sub get_dbturn {
my($dbtcode) = @_;

$readqdbt = "select turn from ttt where code='$dbtcode'";
$rdhdbt = $dbh->prepare($readqdbt);
$rdhdbt->execute or print "Can't read!\n";
return $rdhdbt->fetchrow;
}
###############################################


###############################################
sub get_depends {
my($gdpcode) = @_;
$readquery = "select nodes from ttt_dep where code='$gdpcode'";
$rdh = $dbh->prepare($readquery);
$rdh->execute or print "Can't read!\n";
$gdpstring = $rdh->fetchrow;
@gdp = split(/,/,$gdpstring);
return @gdp;
}
###############################################




######################################## get_triple() #########################################################
sub get_triple {
my($tcode) = @_;
$triple = 0;
@tc = split(//,$tcode);
if ( $tc[0] eq $tc[1] and $tc[1] eq $tc[2] ) {if ($tc[0] eq '1') {$triple = '1';} elsif ($tc[0] eq '2') {$triple = '2';} };
if ( $tc[3] eq $tc[4] and $tc[4] eq $tc[5] ) {if ($tc[3] eq '1') {$triple = '1';} elsif ($tc[3] eq '2') {$triple = '2';} };
if ( $tc[6] eq $tc[7] and $tc[7] eq $tc[8] ) {if ($tc[6] eq '1') {$triple = '1';} elsif ($tc[6] eq '2') {$triple = '2';} };
if ( $tc[0] eq $tc[3] and $tc[3] eq $tc[6] ) {if ($tc[0] eq '1') {$triple = '1';} elsif ($tc[0] eq '2') {$triple = '2';} };
if ( $tc[1] eq $tc[4] and $tc[4] eq $tc[7] ) {if ($tc[1] eq '1') {$triple = '1';} elsif ($tc[1] eq '2') {$triple = '2';} };
if ( $tc[2] eq $tc[5] and $tc[5] eq $tc[8] ) {if ($tc[2] eq '1') {$triple = '1';} elsif ($tc[2] eq '2') {$triple = '2';} };
if ( $tc[0] eq $tc[4] and $tc[4] eq $tc[8] ) {if ($tc[0] eq '1') {$triple = '1';} elsif ($tc[0] eq '2') {$triple = '2';} };
if ( $tc[2] eq $tc[4] and $tc[4] eq $tc[6] ) {if ($tc[2] eq '1') {$triple = '1';} elsif ($tc[2] eq '2') {$triple = '2';} };
return $triple;
                }
######################################## END get_triple() #####################################################


########################################  get_sub_nodes() #########################################################################
sub get_sub_nodes2 {
my($mnode,$gturn) = @_;
if ($gturn eq '') {$gturn = 'X';}

if ($gturn eq 'X') {$turncode = '1';} else {$turncode = '2';}

undef(@subnodes2);

@empty_squares2 = get_empty_squares('',$mnode);
#                            print "Bosluk: ".(scalar(@empty_squares))."\n";
while ($pl = shift(@empty_squares2) ) {
                                        @c = get_coords($pl);
                                        $num = 3*$c[0]+$c[1];
                                        $code = $mnode;
                                        substr($code,$num,1,$turncode);
                                        push(@subnodes2,$code);
                                        }
return @subnodes2;
                    }
############################# END get_sub_nodes() #################################################################################



########################################  get_sub_nodes() #########################################################################
sub get_sub_nodes {
my($mnodes,$gturn) = @_;
if ($gturn eq 'X') {$turncode = '1';} else {$turncode = '2';}
undef(%allsubnodes);

foreach $nd(@{$mnodes}) {
                            #print "$nd\n";
                            $nd = (split(/-/,$nd))[0];
                            #print "$nd\n";
                            undef(@subnodes);

                            @empty_squares = get_empty_squares('',$nd);
#                            print "Bosluk: ".(scalar(@empty_squares))."\n";
                            while ($pl = shift(@empty_squares) ) {
                                                                    @c = get_coords($pl);
                                                                    $num = 3*$c[0]+$c[1];
                                                                    $code = $nd;
                                                                    substr($code,$num,1,$turncode);
                                                                    $code = $code."-$turncode-".($num+1);
                                                                    #$board[$c[0]][$c[1]] = 'X';
                                                                    #$code = get_table_code();
                                                                    push(@subnodes,$code);
                                                                    }
#                                                                    foreach(@subnodes) {print "a $_\n";};
#                                                                    print "---------\n";
                            $allsubnodes{$nd} = [ @subnodes ];
                            #push(%allsubnodes,("$nd",\@subnodes));
                            }
return %allsubnodes;
                    }
############################# END get_sub_nodes() #################################################################################



######################################## get_empty_squares('board','100000000 or table') ################################################
sub get_empty_squares {
my($etable,$getcode) = @_;
if ($etable eq '') {$etable = 'board';}
if ($getcode eq '') {$getcode = 'table';}
undef(@gempty_squares);

if ($getcode eq 'table') {
                            foreach $i (0..2) {
                                            foreach $j (0..2) {
                                                                $number = 3*$i+$j+1;
                                                                if ( ${$etable}[$i][$j] eq $number ) {push(@gempty_squares,$number);}
                                                                }
                                                    }
                            return @gempty_squares;
                            }
else {
        @gcode = split(//,$getcode);
        foreach $i(0..8) {
                        if ($gcode[$i] eq '0') {push(@gempty_squares,$i+1);};
                        }
        return @gempty_squares;
        }
                    }
######################################## END get_empty_squares('board') ############################################




######################################## get_table_code('board') #######################################################
sub get_table_code { #get_table_code('board')
my($ctable) = @_;
if ($ctable eq '') {$ctable = 'board';}
                    $table_code = '';
                    foreach $i (0..2) {
                                    foreach $j (0..2) {
                                                        if (${$ctable}[$i][$j] eq 'X') {$table_code .= '1';}
                                                        elsif (${$ctable}[$i][$j] eq 'O') {$table_code .= '2';}
                                                        else {$table_code .= '0';};
                                                        }
                                        }
return $table_code;
                    }
####################################### END get_table_code() ###########################################################




######################################## get_coords('3') #######################################################
sub get_coords {
my($num) = @_;
undef(@gc);
if ($num eq '1') {@gc = ('0','0');}
if ($num eq '2') {@gc = ('0','1');}
if ($num eq '3') {@gc = ('0','2');}
if ($num eq '4') {@gc = ('1','0');}
if ($num eq '5') {@gc = ('1','1');}
if ($num eq '6') {@gc = ('1','2');}
if ($num eq '7') {@gc = ('2','0');}
if ($num eq '8') {@gc = ('2','1');}
if ($num eq '9') {@gc = ('2','2');}
return @gc;
               }
####################################### END get_coords() ###########################################################



####################################### reset_board() ##############################################################
sub reset_board {
@board = (  ['1','2','3'],
            ['4','5','6'],
            ['7','8','9']   );
}
####################################### reset_board() ##############################################################

####################################### set_board_code($code) ######################################################
sub set_board_code {
}
####################################### set_board_code($code) ######################################################




################################## setxo() #####################################################################
sub setplayerxo {
$komut = `clear`;
print "\n\e[1;29mWhich one will you play? (X or O):\e[0m ";
chomp($playerxo = <STDIN>);
if ($playerxo eq 'x' or $playerxo eq 'X') {$playerxo = 'X';}
elsif ($playerxo eq 'o' or $playerxo eq 'O') {$playerxo = 'O';$turn = 'b';}
else {
        print "Please choose X or O\n";
        setplayerxo();
        }
            }
################################## END setxo() #################################################################



#################################  empty() sub function ############################################
sub checkboard {
undef(@checkb);
@checkb = ();
undef(@fempty);
$chtriple = 0;
            foreach $i(0..2) {
                            foreach $j(0..2) {
                                                if ($board[$i][$j] ne 'X' and $board[$i][$j] ne 'O') {push(@fempty,"$i$j");}
                                                else {
                                                        if ( ($board[$i][$j] eq $board[$i][$j+1]) and ($board[$i][$j+1] eq $board[$i][$j+2]) ) {if ($board[$i][$j] eq 'X') {$chtriple = 1;} elsif ($board[$i][$j] eq 'O') {$chtriple = -1;}};
                                                        if ( ($board[$i][$j] eq $board[$i+1][$j]) and ($board[$i+1][$j] eq $board[$i+2][$j]) ) {if ($board[$i][$j] eq 'X') {$chtriple = 1;} elsif ($board[$i][$j] eq 'O') {$chtriple = -1;}};
                                                        }
                                                }
                            }
if ( ( ($board[1][1] eq 'X' or $board[1][1] eq 'O') and $board[0][0] eq $board[1][1] and $board[1][1] eq $board[2][2]) or ($board[2][0] ne '' and $board[2][0] eq $board[1][1] and $board[1][1] eq $board[0][2]) ) {if ($board[1][1] eq 'X') {$chtriple = 1;} elsif ($board[1][1] eq 'O') {$chtriple = -1;}}

push(@checkb,\@fempty);
push(@checkb,$chtriple);
return @checkb;
                }
#################################  END empty() sub function ############################################




#################################  checktriple() sub function ############################################
sub checktriple {
$checktriple = 0;
                    foreach $i(0..2) {
                                    foreach $j(0..2) {
                                                        if ( ($board[$i][$j] eq $board[$i][$j+1]) and ($board[$i][$j+1] eq $board[$i][$j+2]) ) {$checktriple = 1;}
                                                        if ( ($board[$i][$j] eq $board[$i+1][$j]) and ($board[$i+1][$j] eq $board[$i+2][$j]) ) {$checktriple = 1;}
                                                        }
                                    }
return $checktriple;
                }
#################################  END checktriple() sub function ########################################





######################################## get_table_code('board') #######################################################
sub get_table_code { #get_table_code('board')
my($ctable) = @_;
if ($ctable eq '') {$ctable = 'board';}
                    $table_code = '';
                    foreach $i (0..2) {
                                    foreach $j (0..2) {
                                                        if (${$ctable}[$i][$j] eq 'X') {$table_code .= '1';}
                                                        elsif (${$ctable}[$i][$j] eq 'O') {$table_code .= '2';}
                                                        else {$table_code .= '0';};
                                                        }
                                        }
return $table_code;
                    }
####################################### END get_table_code() ###########################################################




################################## rotate() #########################################################
sub rotate {
undef(@b);

$b[0][0] = $board[0][2];
$b[0][1] = $board[1][2];
$b[0][2] = $board[2][2];

$b[1][0] = $board[0][1];
$b[1][1] = $board[1][1];
$b[1][2] = $board[2][1];

$b[2][0] = $board[0][0];
$b[2][1] = $board[1][0];
$b[2][2] = $board[2][0];

undef(@board);
@board = @b;
undef(@b);
            }
################################## END rotate() #####################################################




################################## checkplace() #########################################################
sub checkplace {
my($pme) = @_;
if ($pme eq 'X') {$pop = 'O';} else {$pop = 'X';};
print "\nMe: $pme , Op: $pop\n";
$temp = '';

# pattern 1
foreach(1..4) {
if ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[0][1] eq $pme and $board[0][2] eq $pme) {$temp = $board[0][0];}
elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[1][0] eq $pme and $board[2][0] eq $pme) {$temp = $board[0][0];}
elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[1][1] eq $pme and $board[2][2] eq $pme) {$temp = $board[0][0];}
elsif ($board[0][1] ne 'X' and $board[0][0] ne 'O' and $board[0][0] eq $pme and $board[0][2] eq $pme) {$temp = $board[0][1];}
elsif ($board[0][1] ne 'X' and $board[0][0] ne 'O' and $board[1][1] eq $pme and $board[2][1] eq $pme) {$temp = $board[0][1];}
rotate();
                }
if ($temp ne ''){print "\nA\n";}

if ($temp eq '') {
                    # pattern 2
                    foreach(1..4) {
                    if ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[0][1] eq $pop and $board[0][2] eq $pop) {$temp = $board[0][0];}
                    elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[1][0] eq $pop and $board[2][0] eq $pop) {$temp = $board[0][0];}
                    elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[1][1] eq $pop and $board[2][2] eq $pop) {$temp = $board[0][0];}
                    elsif ($board[0][1] ne 'X' and $board[0][1] ne 'O' and $board[0][0] eq $pop and $board[0][2] eq $pop) {$temp = $board[0][1];}
                    elsif ($board[0][1] ne 'X' and $board[0][1] ne 'O' and $board[1][1] eq $pop and $board[2][1] eq $pop) {$temp = $board[0][1];}
                    rotate();
                                    }

                    if ($temp ne ''){print "\nB\n";}

                    if($temp eq '') {
                                        # pattern 3
                                        foreach(1..4) {
                                        if ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[0][1] ne 'X' and $board[0][1] ne 'O'and $board[1][0] ne 'X' and $board[1][0] ne 'O' and $board[1][2] ne 'X' and $board[1][2] ne 'O' and $board[2][1] ne 'X' and $board[2][1] ne 'O' and $board[2][2] ne 'X' and $board[2][2] ne 'O'
                                        and $board[2][0] eq $pop and $board[1][1] eq $pme and $board[0][2] eq $pop ) {
                                                                                                                    $temp = $board[0][1];
                                                                                                                    }
                                        elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[0][2] ne 'X' and $board[0][2] ne 'O' and $board[2][0] ne 'X' and $board[2][0] ne 'O'
                                        and $board[1][0] eq $pop and $board[0][1] eq $pop and $board[1][1] eq $pme) {
                                                                                                                $temp = $board[0][0];
                                                                                                                }
                                        elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[0][1] ne 'X' and $board[0][1] ne 'O' and $board[2][0] ne 'X' and $board[2][0] ne 'O'
                                        and $board[1][0] eq $pop and $board[0][2] eq $pop and $board[1][1] eq $pme) {
                                                                                                                $temp = $board[0][0];
                                                                                                                }
                                        elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[0][1] ne 'X' and $board[0][1] ne 'O' and $board[1][0] ne 'X' and $board[1][0] ne 'O'
                                        and $board[2][0] eq $pop and $board[1][1] eq $pme and $board[0][2] eq $pop) {
                                                                                                                $temp = $board[0][0];
                                                                                                                }
                                        elsif ($board[0][1] ne 'X' and $board[0][1] ne 'O' and $board[2][1] ne 'X' and $board[2][1] ne 'O'
                                        and $board[1][1] eq $pop) {
                                                                                                                $temp = $board[0][1];
                                                                                                                }
                                        elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[2][2] ne 'X' and $board[2][2] ne 'O'
                                        and $board[1][1] eq $pop) {
                                                                                                                $temp = $board[0][0];
                                                                                                                }
                                        rotate();
                                                    }

                                        if ($temp ne ''){print "\nC\n";}
                                        if ($temp eq '') {
                                                            foreach(1..4) {
                                                                            if ($temp eq '') {
                                                                                if ($board[0][0] ne 'X' and $board[1][1] ne 'O' and $board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[0][1] ne 'X' and $board[0][1] ne 'O') {print "1\n";@br = ("$board[1][1]","$board[0][0]","$board[0][1]");$temp = $br[int rand(scalar(@br))];}
                                                                                elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[0][1] ne 'X' and $board[0][1] ne 'O') {print "2\n";@br = ("$board[0][0]","$board[0][1]");$temp = $br[int rand(scalar(@br))];}
                                                                                elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O' and $board[1][1] ne 'X' and $board[1][1] ne 'O') {print "3\n";@br = ("$board[0][0]","$board[1][1]");$temp = $br[int rand(scalar(@br))];}
                                                                                elsif ($board[0][1] ne 'X' and $board[0][1] ne 'O' and $board[1][1] ne 'X' and $board[1][1] ne 'O') {print "4\n";@br = ("$board[0][1]","$board[1][1]");$temp = $br[int rand(scalar(@br))];}
                                                                                elsif ($board[1][1] ne 'X' and $board[1][1] ne 'O') {$temp = $board[1][1];}
                                                                                elsif ($board[0][1] ne 'X' and $board[0][1] ne 'O') {$temp = $board[0][1];}
                                                                                elsif ($board[0][0] ne 'X' and $board[0][0] ne 'O') {$temp = $board[0][0];}
                                                                                            }
                                                                            rotate();
                                                                            }
                                                            if ($temp ne ''){print "\nD\n";}
                                                            }
                                    }
                }
return $temp;
        }
################################## END checkplace() #####################################################





################ automove($t) ##################################
sub automove { #automove($t)
my($t) = @_;
$newplace = '';

if ($t eq 'b') {if ($playerxo eq 'X') {$me = 'O';} else {$me = 'X';}}
else {$me = $playerxo;};

$nowcode = get_table_code();

undef(@allmoves);
@allmoves = get_sub_nodes2($nowcode,'O');

undef(%validmoves);

print "\n-----------------\n";
foreach $key (@allmoves) {  chomp($key);

                            $tval = ${$init}{$key}{'value'};
                            print "$key (".compare_codes($key,$nowcode).") = $tval\n";
                            $validmoves{$key} = $tval;
                            }

@keysvalid = (sort hashValueAscendingNum3 (keys(%validmoves)));

if (scalar(@mateseq) > 0) {$secim = @keysvalid[0];}

else {
        $minimum = $validmoves{$keysvalid[0]};

        undef(@bestmoves);
        foreach $vkey (@keysvalid) {
                                    if ($validmoves{$vkey} == $minimum) {push(@bestmoves,$vkey);}
                                    }
        $secim = $bestmoves[int rand(scalar(@bestmoves))];
        }
$aplace = compare_codes($secim,$nowcode);
$gamelist .= $aplace.":";

print "-----------------";
print "\nPondering...... ";

$newplace = convertplace($aplace);

if ($t eq 'w') {$yazilacak = $playerxo;} else {if ($playerxo eq 'X') {$yazilacak = 'O';} else {$yazilacak = 'X';} };

$board[(split(//,$newplace))[0]][(split(//,$newplace))[1]] = $yazilacak;

$countmove++;

if ($t eq 'w') {print "\e[0;0;44m$aplace\e[0m\n";$turn = 'b';}
else {print "\e[0;0;41m$aplace\e[0m\n";$turn = 'w';}
#$dur = <STDIN>;
	}
################ END OF automove ($t) ##########################


################### compare_codes() ###############################
sub compare_codes {
my($code1,$code2) = @_;
@c1 = split(//,$code1);
@c2 = split(//,$code2);
for($i=0;$i<9;$i++) {
                    if ($c1[$i] ne $c2[$i]) {return $i+1;};
                    }
}
################### compare_codes() ###############################





########################### qtable() ################################################
sub qtable { #qtable()

my($mate) = @_;

$currentcode = get_table_code();
#$dur = <STDIN>;

if ($countmove >= 1) {
                        ###################################################################################################################
                        @next_state_all_moves = get_sub_nodes2($currentcode);
                        undef(%nextmovespastvalues);
                        %nextmovespastvalues = ();
                        foreach (@next_state_all_moves) {
                                                        if ( defined($q{$currentcode}{$_}) ) {
                                                                                            $nextmovespastvalues{$_} = $q{$currentcode}{$_};
                                                                                            };
                                                        };
                        @keynext = (sort hashValueAscendingNum2 (keys(%nextmovespastvalues)));
                        $max_v_next_state = $nextmovespastvalues{$keynext[-1]};
                        $oldq = $q{$lastcodelist[-1]}{$whitenodelist[-1]};
                        ###################################################################################################################

        # eger qtable da fen ve move ikilisi icin daha once kayit yoksa yeni bir satir olarak veri giriliyor. $q{fen}{move} = q
        if ( $q{$lastcodelist[-1]}{$whitenodelist[-1]} eq '') {
                                                                if ($mate eq 'mate') {
                                                                                    $newq = (1-$alfa)*0+$alfa*($reward+$beta*$max_v_next_state);
                                                                                    print "Adding new record ($lastcodelist[-1]:$whitenodelist[-1])...(MATE)\n";#$dur = <STDIN>;
                                                                                    $q{$lastcodelist[-1]}{$whitenodelist[-1]} = $newq;
                                                                                    $addquery = "insert into qtable_ttt(id,code,move,value) values('','$lastcodelist[-1]','$whitenodelist[-1]','$newq')";
                                                                                    $adh = $dbh->prepare($addquery);
                                                                                    $adh->execute or print "Can't insert record!\n";

                                                                                    $readquery = "select value from qtable_ttt where code='$lastcodelist[-1]' and move='$whitenodelist[-1]'";
                                                                                    $rdh = $dbh->prepare($readquery);
                                                                                    $rdh->execute or print "Can't read!\n";
                                                                                    $a = $rdh->fetchrow;
                                                                                    #print "A: $a\n";

                                                                                    $search = "$lastcodelist[-1]:$whitenodelist[-1]";
                                                                                    my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                                                                    print "kayitin oyun sira indexi: $index\n";

                                                                                    if ($index > 0 and $newmethod eq '1') {
                                                                                    print "Ayrica asagidakiler guncellenecektir:\n";

                                                                                    for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                                                                                print "$seq[$i]\n-------------\n";
                                                                                                                print "\nNext State : ".($seq[$i+1])."\n";
                                                                                                                @thisarr = split(/:/,$seq[$i]);
                                                                                                                @nextarr = split(/:/,$seq[$i+1]);
                                                                                                                print "Nextcode : $nextarr[0]\n";

                                                                                                                $maxvquery = "select code,move,value from qtable_ttt where code='$nextarr[0]' order by value desc limit 1";
                                                                                                                $mdh = $dbh->prepare($maxvquery);
                                                                                                                $mdh->execute or print "Can't read!\n";
                                                                                                                @a = $mdh->fetchrow_array();

                                                                                                                print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                                                                $max_v_next_state1 = $a[2];
                                                                                                                $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                                                                print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                                                                $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                                                                $updatequery = "update qtable_ttt set value='$newq' where code='$thisarr[0]' and move='$thisarr[1]'";
                                                                                                                $udh = $dbh->prepare($updatequery);
                                                                                                                $udh->execute or print "Can't update!\n";
                                                                                                                }
                                                                                                    }
                                                                                    open(yeni,"+>> qtable_ttt.db");
                                                                                    print yeni "$lastcodelist[-1]:$whitenodelist[-1]:$q{$lastcodelist[-1]}{$whitenodelist[-1]}\n";
                                                                                    $komut = `echo 'Adding ---> $lastcodelist[-1]:$whitenodelist[-1]:$q{$lastcodelist[-1]}{$whitenodelist[-1]}' >> added_list_ttt`;
                                                                                    close(yeni);
                                                                                    }
                                                                elsif ($mate eq 'draw') {
                                                                                    $newq = (1-$alfa)*0+$alfa*($reward+$beta*$max_v_next_state);
                                                                                    print "Adding new record ($lastcodelist[-1]:$whitenodelist[-1])...(DRAW)\n";#$dur = <STDIN>;
                                                                                    $q{$lastcodelist[-1]}{$whitenodelist[-1]} = $newq;

                                                                                    $addquery = "insert into qtable_ttt(id,code,move,value) values('','$lastcodelist[-1]','$whitenodelist[-1]','$newq')";
                                                                                    $adh = $dbh->prepare($addquery);
                                                                                    $adh->execute or print "Can't insert!\n";

                                                                                    $readquery = "select value from qtable_ttt where code='$lastcodelist[-1]' and move='$whitenodelist[-1]'";
                                                                                    $rdh = $dbh->prepare($readquery);
                                                                                    $rdh->execute or print "Can't read!\n";
                                                                                    $a = $rdh->fetchrow;
                                                                                    #print "B: $a\n";

                                                                                    $search = "$lastcodelist[-1]:$whitenodelist[-1]";
                                                                                    my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                                                                    print "kayitin oyun sira indexi: $index\n";

                                                                                    if ($index > 0 and $newmethod eq '1') {
                                                                                    print "Ayrica asagidakiler guncellenecektir:\n";

                                                                                    for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                                                                                print "$seq[$i]\n-------------\n";
                                                                                                                print "\nNext State : ".($seq[$i+1])."\n";
                                                                                                                @thisarr = split(/:/,$seq[$i]);
                                                                                                                @nextarr = split(/:/,$seq[$i+1]);
                                                                                                                print "Nextcode : $nextarr[0]\n";

                                                                                                                $maxvquery = "select code,move,value from qtable_ttt where code='$nextarr[0]' order by value desc limit 1";
                                                                                                                $mdh = $dbh->prepare($maxvquery);
                                                                                                                $mdh->execute or print "Can't read!\n";
                                                                                                                @a = $mdh->fetchrow_array();

                                                                                                                print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                                                                $max_v_next_state1 = $a[2];
                                                                                                                $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                                                                print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                                                                $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                                                                $updatequery = "update qtable_ttt set value='$newq' where code='$thisarr[0]' and move='$thisarr[1]'";
                                                                                                                $udh = $dbh->prepare($updatequery);
                                                                                                                $udh->execute or print "Can't update!\n";
                                                                                                                }
                                                                                                    }
                                                                                    open(yeni,"+>> qtable_ttt.db");
                                                                                    print yeni "$lastcodelist[-1]:$whitenodelist[-1]:$q{$lastcodelist[-1]}{$whitenodelist[-1]}\n";
                                                                                    $komut = `echo 'Adding ---> $lastcodelist[-1]:$whitenodelist[-1]:$q{$lastcodelist[-1]}{$whitenodelist[-1]}' >> added_list_ttt`;
                                                                                    close(yeni);
                                                                                        }
                                                                else {
                                                                        $newq = (1-$alfa)*$q{$lastcodelist[-1]}{$whitenodelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                                                                        print "Adding new record ($lastcodelist[-1]:$whitenodelist[-1])...\n";#$dur = <STDIN>;
                                                                        $q{$lastcodelist[-1]}{$whitenodelist[-1]} = $newq;

                                                                        $addquery = "insert into qtable_ttt(id,code,move,value) values('','$lastcodelist[-1]','$whitenodelist[-1]','$newq')";
                                                                        $adh = $dbh->prepare($addquery);
                                                                        $adh->execute or print "Can't insert!\n";

                                                                        $readquery = "select value from qtable_ttt where code='$lastcodelist[-1]' and move='$whitenodelist[-1]'";
                                                                        $rdh = $dbh->prepare($readquery);
                                                                        $rdh->execute or print "Can't read!\n";
                                                                        $a = $rdh->fetchrow;
                                                                        #print "C: $a\n";

                                                                        $search = "$lastcodelist[-1]:$whitenodelist[-1]";
                                                                        my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                                                        print "kayitin oyun sira indexi: $index\n";

                                                                        if ($index > 0 and $newmethod eq '1') {
                                                                        print "Ayrica asagidakiler guncellenecektir:\n";


                                                                        for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                                                                    print "$seq[$i]\n-------------\n";
                                                                                                    print "\nNext State : ".($seq[$i+1])."\n";
                                                                                                    @thisarr = split(/:/,$seq[$i]);
                                                                                                    @nextarr = split(/:/,$seq[$i+1]);
                                                                                                    print "Nextcode : $nextarr[0]\n";

                                                                                                    $maxvquery = "select code,move,value from qtable_ttt where code='$nextarr[0]' order by value desc limit 1";
                                                                                                    $mdh = $dbh->prepare($maxvquery);
                                                                                                    $mdh->execute or print "Can't read!\n";
                                                                                                    @a = $mdh->fetchrow_array();

                                                                                                    print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                                                    $max_v_next_state1 = $a[2];
                                                                                                    $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                                                    print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                                                    $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                                                    $updatequery = "update qtable_ttt set value='$newq' where code='$thisarr[0]' and move='$thisarr[1]'";
                                                                                                    $udh = $dbh->prepare($updatequery);
                                                                                                    $udh->execute or print "Can't update!\n";
                                                                                                    }
                                                                                        }
                                                                        open(yeni,"+>> qtable_ttt.db");
                                                                        print yeni "$lastcodelist[-1]:$whitenodelist[-1]:$q{$lastcodelist[-1]}{$whitenodelist[-1]}\n";
                                                                        $komut = `echo 'Adding ---> $lastcodelist[-1]:$whitenodelist[-1]:$q{$lastcodelist[-1]}{$whitenodelist[-1]}' >> added_list_ttt`;
                                                                        close(yeni);
                                                                        }
                                                                }
        else {
                if ($mate eq 'mate') {
                                    print "Updating record ($lastcodelist[-1]:$whitenodelist[-1])...(MATE)\n";#$dur = <STDIN>;
                                    $newq = (1-$alfa)*$q{$lastcodelist[-1]}{$whitenodelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                                    $q{$lastcodelist[-1]}{$whitenodelist[-1]} = $newq;

                                    $oldline = "$lastcodelist[-1]:$whitenodelist[-1]:$oldq";
                                    $oldline1 = $oldline;
                                    $newline = "$lastcodelist[-1]:$whitenodelist[-1]:$newq";
                                    $newline1 = $newline;

                                    $oldline =~ s/\//\\\//g;
                                    $newline =~ s/\//\\\//g;

                                    $komut1 = `mv qtable_ttt.db tempdb_ttt.db`;
                                    $sed = `sed 's/$oldline/$newline/g' tempdb_ttt.db > qtable_ttt.db`;

                                    $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list_ttt`;
                                    $komut = `echo 'Updating ($oldline1) (MATE)...' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;29mOldQ\e[0m = \e[1;41m$oldq\e[0m' >> updated_list_ttt`;
                                    $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                                    $komut = `echo 'Previously visited (NextState,VisitedMoves) :' >> updated_list_ttt`;
                                    $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                                    foreach(@keynext) {
                                                        $komut = `echo '($currentcode) , \e[1;29m$_\e[0m = \e[1;32m$nextmovespastvalues{$_}\e[0m' >> updated_list_ttt`;
                                                        }
                                    $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;34mMaxQnext = $max_v_next_state\e[0m' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;31mCalculating...:\e[0m\e[1;29m (1-$alfa)*$oldq+$alfa*($reward+$beta*$max_v_next_state) = $newq\e[0m' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;29mNewQ\e[0m = \e[1;44m$newq\e[0m' >> updated_list_ttt`;
                                    $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                                    $komut = `echo '---> ($newline1) MAX: $max_v_next_state' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list_ttt`;

                                    $updatequery = "update qtable_ttt set value='$newq' where code='$lastcodelist[-1]' and move='$whitenodelist[-1]'";
                                    $adh = $dbh->prepare($updatequery);
                                    $adh->execute or print "Can't update!\n";

                                    $readquery = "select value from qtable_ttt where code='$lastcodelist[-1]' and move='$whitenodelist[-1]'";
                                    $rdh = $dbh->prepare($readquery);
                                    $rdh->execute or print "Can't read!\n";
                                    $a = $rdh->fetchrow;
                                    #print "D: $a\n";

                                    $search = "$lastcodelist[-1]:$whitenodelist[-1]";
                                    my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                    print "kayitin oyun sira indexi: $index\n";

                                    if ($index > 0 and $newmethod eq '1') {
                                    print "Ayrica asagidakiler guncellenecektir:\n";


                                    for($i=$index-1;$i>=0;$i--) {
                                                                $rewardback = 0;
                                                                print "$seq[$i]\n-------------\n";
                                                                print "\nNext State : ".($seq[$i+1])."\n";
                                                                @thisarr = split(/:/,$seq[$i]);
                                                                @nextarr = split(/:/,$seq[$i+1]);
                                                                print "Nextcode : $nextarr[0]\n";

                                                                $maxvquery = "select code,move,value from qtable_ttt where code='$nextarr[0]' order by value desc limit 1";
                                                                $mdh = $dbh->prepare($maxvquery);
                                                                $mdh->execute or print "Can't read!\n";
                                                                @a = $mdh->fetchrow_array();

                                                                print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                $max_v_next_state1 = $a[2];
                                                                $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                $updatequery = "update qtable_ttt set value='$newq' where code='$thisarr[0]' and move='$thisarr[1]'";
                                                                $udh = $dbh->prepare($updatequery);
                                                                $udh->execute or print "Can't update!\n";
                                                                }
                                                    }
                                        }

                elsif ($mate eq 'draw') {
                                    print "Updating record ($lastcodelist[-1]:$whitenodelist[-1])...(DRAW)\n";#$dur = <STDIN>;
                                    $newq = (1-$alfa)*$q{$lastcodelist[-1]}{$whitenodelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                                    print "OLD: $oldq\nNEW: $newq\n";
                                    $q{$lastcodelist[-1]}{$whitenodelist[-1]} = $newq;

                                    $oldline = "$lastcodelist[-1]:$whitenodelist[-1]:$oldq";
                                    $oldline1 = $oldline;
                                    $newline = "$lastcodelist[-1]:$whitenodelist[-1]:$newq";
                                    $newline1 = $newline;

                                    $oldline =~ s/\//\\\//g;
                                    $newline =~ s/\//\\\//g;

                                    $komut1 = `mv qtable_ttt.db tempdb_ttt.db`;
                                    $sed = `sed 's/$oldline/$newline/g' tempdb_ttt.db > qtable_ttt.db`;

                                    $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list_ttt`;
                                    $komut = `echo 'Updating ($oldline1) (DRAW)...' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;29mOldQ\e[0m = \e[1;41m$oldq\e[0m' >> updated_list_ttt`;
                                    $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                                    $komut = `echo 'Previously visited (NextState,VisitedMoves) :' >> updated_list_ttt`;
                                    $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                                    foreach(@keynext) {
                                                        $komut = `echo '($currentcode) , \e[1;29m$_\e[0m = \e[1;32m$nextmovespastvalues{$_}\e[0m' >> updated_list_ttt`;
                                                        }
                                    $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;34mMaxQnext = $max_v_next_state\e[0m' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;31mCalculating...:\e[0m\e[1;29m (1-$alfa)*$oldq+$alfa*($reward+$beta*$max_v_next_state) = $newq\e[0m' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;29mNewQ\e[0m = \e[1;44m$newq\e[0m' >> updated_list_ttt`;
                                    $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                                    $komut = `echo '---> ($newline1) MAX: $max_v_next_state' >> updated_list_ttt`;
                                    $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list_ttt`;

                                    $updatequery = "update qtable_ttt set value='$newq' where code='$lastcodelist[-1]' and move='$whitenodelist[-1]'";
                                    $adh = $dbh->prepare($updatequery);
                                    $adh->execute or print "Can't update!\n";

                                    $readquery = "select value from qtable_ttt where code='$lastcodelist[-1]' and move='$whitenodelist[-1]'";
                                    $rdh = $dbh->prepare($readquery);
                                    $rdh->execute or print "Can't read!\n";
                                    $a = $rdh->fetchrow;
                                    #print "E: $a\n";

                                    $search = "$lastcodelist[-1]:$whitenodelist[-1]";
                                    my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                                    print "kayitin oyun sira indexi: $index\n";

                                    if ($index > 0 and $newmethod eq '1') {
                                    print "Ayrica asagidakiler guncellenecektir:\n";


                                    for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                                print "$seq[$i]\n-------------\n";
                                                                print "\nNext State : ".($seq[$i+1])."\n";
                                                                @thisarr = split(/:/,$seq[$i]);
                                                                @nextarr = split(/:/,$seq[$i+1]);
                                                                print "Nextcode : $nextarr[0]\n";

                                                                $maxvquery = "select code,move,value from qtable_ttt where code='$nextarr[0]' order by value desc limit 1";
                                                                $mdh = $dbh->prepare($maxvquery);
                                                                $mdh->execute or print "Can't read!\n";
                                                                @a = $mdh->fetchrow_array();

                                                                print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                $max_v_next_state1 = $a[2];
                                                                $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                $updatequery = "update qtable_ttt set value='$newq' where code='$thisarr[0]' and move='$thisarr[1]'";
                                                                $udh = $dbh->prepare($updatequery);
                                                                $udh->execute or print "Can't update!\n";
                                                                }
                                                    }
                                        }

                else {
                        print "Updating record ($lastcodelist[-1]:$whitenodelist[-1])...\n";#$dur = <STDIN>;
                        # QTABLE HESABI burada yapiliyor
                        $newq = (1-$alfa)*$q{$lastcodelist[-1]}{$whitenodelist[-1]}+$alfa*($reward+$beta*$max_v_next_state);
                        $q{$lastcodelist[-1]}{$whitenodelist[-1]} = $newq;

                        $oldline = "$lastcodelist[-1]:$whitenodelist[-1]:$oldq";
                        $oldline1 = $oldline;
                        $newline = "$lastcodelist[-1]:$whitenodelist[-1]:$newq";
                        $newline1 = $newline;

                        $oldline =~ s/\//\\\//g;
                        $newline =~ s/\//\\\//g;

                        $komut1 = `mv qtable_ttt.db tempdb_ttt.db`;
                        $sed = `sed 's/$oldline/$newline/g' tempdb_ttt.db > qtable_ttt.db`;

                        $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list_ttt`;
                        $komut = `echo 'Updating ($oldline1)...' >> updated_list_ttt`;
                        $komut = `echo '\e[1;29mOldQ\e[0m = \e[1;41m$oldq\e[0m' >> updated_list_ttt`;
                        $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                        $komut = `echo 'Previously visited (NextState,VisitedMoves) :' >> updated_list_ttt`;
                        $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                        foreach(@keynext) {
                                            $komut = `echo '($currentcode) , \e[1;29m$_\e[0m = \e[1;32m$nextmovespastvalues{$_}\e[0m' >> updated_list_ttt`;
                                            }
                        $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                        $komut = `echo '\e[1;34mMaxQnext = $max_v_next_state\e[0m' >> updated_list_ttt`;
                        $komut = `echo '\e[1;31mCalculating...:\e[0m\e[1;29m (1-$alfa)*$oldq+$alfa*($reward+$beta*$max_v_next_state) = $newq\e[0m' >> updated_list_ttt`;
                        $komut = `echo '\e[1;29mNewQ\e[0m = \e[1;44m$newq\e[0m' >> updated_list_ttt`;
                        $komut = `echo '-------------------------------------' >> updated_list_ttt`;
                        $komut = `echo '---> ($newline1) MAX: $max_v_next_state' >> updated_list_ttt`;
                        $komut = `echo '\e[1;33m--------------------------------------------------------------------------------------------------------\e[0m' >> updated_list_ttt`;

                        $updatequery = "update qtable_ttt set value='$newq' where code='$lastcodelist[-1]' and move='$whitenodelist[-1]'";
                        $adh = $dbh->prepare($updatequery);
                        $adh->execute or print "Can't update!\n";

                        $readquery = "select value from qtable_ttt where code='$lastcodelist[-1]' and move='$whitenodelist[-1]'";
                        $rdh = $dbh->prepare($readquery);
                        $rdh->execute or print "Can't read!\n";
                        $a = $rdh->fetchrow;
                        #print "F: $a\n";

                        $search = "$lastcodelist[-1]:$whitenodelist[-1]";
                        my $index = List::MoreUtils::first_index {$_ eq $search} @seq;
                        print "kayitin oyun sira indexi: $index\n";

                        if ($index > 0 and $newmethod eq '1') {
                        print "\nAyrica asagidakiler guncellenecektir:\n";

                                                                for($i=$index-1;$i>=0;$i--) {$rewardback = 0;
                                                                                            print "\n$seq[$i]\n-------------\n";
                                                                                            print "\nNext State : ".($seq[$i+1])."\n";
                                                                                            @thisarr = split(/:/,$seq[$i]);
                                                                                            @nextarr = split(/:/,$seq[$i+1]);
                                                                                            print "Nextcode : $nextarr[0]\n";

                                                                                            $maxvquery = "select code,move,value from qtable_ttt where code='$nextarr[0]' order by value desc limit 1";
                                                                                            $mdh = $dbh->prepare($maxvquery);
                                                                                            $mdh->execute or print "Can't read!\n";
                                                                                            @a = $mdh->fetchrow_array();

                                                                                            print "MaxQnext: $a[0] , $a[1] , $a[2]\n";
                                                                                            $max_v_next_state1 = $a[2];
                                                                                            $newq = (1-$alfa)*$q{$thisarr[0]}{$thisarr[1]}+$alfa*($rewardback+$beta*$max_v_next_state1);
                                                                                            print "Old: $q{$thisarr[0]}{$thisarr[1]} , New: $newq\n";
                                                                                            $q{$thisarr[0]}{$thisarr[1]} = $newq;

                                                                                            $updatequery = "update qtable_ttt set value='$newq' where code='$thisarr[0]' and move='$thisarr[1]'";
                                                                                            $udh = $dbh->prepare($updatequery);
                                                                                            $udh->execute or print "Can't update!\n";
                                                                                            }
                                                                }
                        }
            }
        }
#$dur = <STDIN>;
    }
########################### END qtable() ############################################





###### 1-> learning (Agent vs. Computer(minmax))  (without new method)
###### 2-> learning (Agent vs. Computer(minmax))  (with new method)
###### 3-> gaming   (Agent vs. Computer(minmax))
###### 4-> gaming   (Player vs Agent)
###### 5-> gaming   (Player vs. Computer(minmax))
###### 6-> gaming   (Player vs. Player)

################# giristeki komut listesini yazar ###########################################
sub commandlist { #commandlist();
print "(\e[1;31m1\e[0m) \e[0;0;4mLearning\e[0m        -> Agent will play against computer and learn (writes to qtable_ttt db.).\n";
print "(\e[1;31m2\e[0m) \e[0;0;4mLearning\e[0m (N.M.) -> Agent will play against computer and learn (with new method) (writes to qtable_ttt db.).\n";
print "(\e[1;31m3\e[0m) \e[0;0;4mAgent vs. Comp.\e[0m -> Agent plays against computer (MinMax) (reads from qtable_ttt db.).\n";
print "(\e[1;31m4\e[0m) \e[0;0;4mPlay. vs. Agent\e[0m -> Play against agent (reads from qtable_ttt db.).\n";
print "(\e[1;31m5\e[0m) \e[0;0;4mPlay. vs. Comp.\e[0m -> Play against computer (MinMax).\n";
print "(\e[1;31m6\e[0m) \e[0;0;4mPlay. vs. Play.\e[0m -> Play against another player.\n";
                }
################ end commandlist() ##########################################################







#################### convertplace($pl) ###########################################################
sub convertplace {
my($pl) = @_;
$plc = '';
#print "CONVERT : $pl\n";
if ($pl eq '1') {$plc = '00';}
elsif ($pl eq '2') {$plc = '01';}
elsif ($pl eq '3') {$plc = '02';}
elsif ($pl eq '4') {$plc = '10';}
elsif ($pl eq '5') {$plc = '11';}
elsif ($pl eq '6') {$plc = '12';}
elsif ($pl eq '7') {$plc = '20';}
elsif ($pl eq '8') {$plc = '21';}
elsif ($pl eq '9') {$plc = '22';}
return $plc;
                    }
#################### END convertplace() #######################################################







################################# mate() ###################################################################################
sub mate {
if ($turn eq 'b') {
                    print "\e[1;31mBlack\e[0m";
                    }
else {print "\e[1;29mWhite\e[0m";};
print " (".(($countmove-($countmove%2))/2).") \e[1;5;35mWINS!\e[0m\n\n";

if ($turn eq 'w'){
                    $reward = -$rewardpoint;
                    }
elsif ($turn eq 'b') {
                        $reward = $rewardpoint;
                        }
if ($learnmode == 1) {
                        qtable('mate');
                        open(GAMELIST,"+>> gamelist_ttt.list");
                        print GAMELIST "$countmove->$gamelist\mate->$turn\n";
                        close(GAMELIST);
                        }
startposition();
moveselect();
}
############################################################################################################################






################################# draw() ####################################################################################
sub draw {

if ($learnmode == 1) {
                        }

print "GAME was \e[1;5;36mDRAW!\e[0m\n\n";

$reward = 0;

if ($learnmode == 1) {
                        qtable('draw');
                        open(GAMELIST,"+>> gamelist_ttt.list");
                        print GAMELIST "$countmove->$gamelist\draw\n";
                        close(GAMELIST);
                        }

startposition();
moveselect()
}
################################# END draw() ################################################################################





# hash yapilarda siralama icin gereken alt fonksiyonlar
##################################################################################
sub hashValueAscendingNum {
    $allqmoves{$a} <=> $allqmoves{$b};
}

sub hashValueDescendingNum {
    $allqmoves{$b} <=> $allqmoves{$a};
}
##################################################################################

##################################################################################
sub hashValueAscendingNum1 {
    $gamemodemoves{$a} <=> $gamemodemoves{$b};
}

sub hashValueDescendingNum1 {
    $gamemodemoves{$b} <=> $gamemodemoves{$a};
}
##################################################################################

##################################################################################
sub hashValueAscendingNum2 {
    $nextmovespastvalues{$a} <=> $nextmovespastvalues{$b};
}

sub hashValueDescendingNum2 {
    $nextmovespastvalues{$b} <=> $nextmovespastvalues{$a};
}
##################################################################################

##################################################################################
sub hashValueAscendingNum3 {
    $validmoves{$a} <=> $validmoves{$b};
}

sub hashValueDescendingNum3 {
    $validmoves{$b} <=> $validmoves{$a};
}
##################################################################################



end:
